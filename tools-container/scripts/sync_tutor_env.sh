#!/usr/bin/env bash

set -e

CURRENT_DIR=${BASH_SOURCE%/*}

if [[ -z "${TF_VAR_cluster_provider}" ]]; then
    echo "Please set the 'TF_VAR_cluster_provider' env variable. Options are 'aws', 'digitalocean'"
    exit 1
fi

if [[ -z "${TUTOR_ROOT}" ]]; then
    echo "Please set the 'TUTOR_ROOT' env variable with the full path to the tutor project"
    exit 1
fi

if [[ -z "${TUTOR_ID}" ]]; then
    echo "Please set the 'TUTOR_ID' env variable name of the tutor project"
    exit 1
fi

EXTRA_ARGS=""

if [ "${TF_VAR_cluster_provider}" == "digitalocean" ]; then

    if [[ -z "${TF_VAR_do_region}" ]]; then
        echo "Please set the 'TF_VAR_do_region' env variable"
        exit 1
    fi

    export AWS_ACCESS_KEY_ID=$SPACES_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$SPACES_SECRET_ACCESS_KEY
    EXTRA_ARGS="--endpoint=https://${TF_VAR_do_region}.digitaloceanspaces.com"
fi

if [ "${TF_VAR_cluster_provider}" == "aws" ]; then

    if [[ -z "${TF_VAR_aws_region}" ]]; then
        echo "Please set the 'TF_VAR_aws_region' env variable"
        exit 1
    fi

    EXTRA_ARGS="--region ${TF_VAR_aws_region}"
fi

# get s3 bucket name for tutor env folder
ENV_BUCKET_NAME=`bash $CURRENT_DIR/tf.sh output -json tutor_env_bucket | tr -d '"'`

ENV_DIRECTORY=${TUTOR_ROOT}/env

PRESERVED_CONFIG_FILENAME="grove-preserved-config.yml"

function download_preserved_config {
    # download preserved config file from s3 if exists and save a copy

    local local_path=$TUTOR_ROOT/$PRESERVED_CONFIG_FILENAME
    local remote_path=s3://${ENV_BUCKET_NAME}/${TUTOR_ID}/$PRESERVED_CONFIG_FILENAME

    if aws s3 ls $remote_path ${EXTRA_ARGS} > /dev/null; then
        aws s3 cp $remote_path $local_path ${EXTRA_ARGS}
        cp $local_path $local_path.previous
    fi
}

function upload_preserved_config {
    # upload preserved config file to s3 if exists and has changed since last upload

    local local_path=$TUTOR_ROOT/$PRESERVED_CONFIG_FILENAME
    local remote_path=s3://${ENV_BUCKET_NAME}/${TUTOR_ID}/$PRESERVED_CONFIG_FILENAME

    if [ -f $local_path ]; then
        if cmp -s $local_path.previous $local_path; then
            echo "No changes in $PRESERVED_CONFIG_FILENAME"
        else
            aws s3 cp $local_path $remote_path ${EXTRA_ARGS}
            aws s3 cp $remote_path $remote_path.$(date +%Y%m%d%H%M%S) ${EXTRA_ARGS}
        fi
    fi
}

if [ $1 == "pull" ]; then
    # download env directory from s3 if exists
    aws s3 sync s3://${ENV_BUCKET_NAME}/${TUTOR_ID}/env $ENV_DIRECTORY ${EXTRA_ARGS}
    download_preserved_config
elif [ $1 == "push" ]; then
    # upload updated env directory to s3
    aws s3 sync $ENV_DIRECTORY s3://${ENV_BUCKET_NAME}/${TUTOR_ID}/env ${EXTRA_ARGS} --delete
    upload_preserved_config
else
    echo "Invalid operation!"
fi
