import os
from pathlib import Path

WORKSPACE_DIR = Path(os.environ.get("CI_PROJECT_DIR", "/workspace"))
INSTANCES_DIR = WORKSPACE_DIR / "instances"
GROVE_DEFAULTS_DIR = WORKSPACE_DIR / "defaults"
GROVE_TEMPLATES_DIR = Path(__file__).parent.absolute() / "templates"
TOOLS_CONTAINER_DIR = Path(__file__).parent.parent.parent.absolute()
SCRIPTS_DIR = TOOLS_CONTAINER_DIR / "scripts"
CI_SCRIPTS_DIR = TOOLS_CONTAINER_DIR / "ci_scripts"
CI_REGISTRY_IMAGE = os.getenv("CI_REGISTRY_IMAGE")
VENV_DIR_NAME = ".venv"
