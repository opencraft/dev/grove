"""
Utility functions for k8s management.
"""
from datetime import datetime

from kubernetes import client, config

from grove.utils import execute


def get_v1apps_api() -> client.AppsV1Api:
    """
    Load the given kubeconfig and returns an AppsV1Api client.
    """
    config.load_kube_config()
    return client.AppsV1Api()


def restart_deployment(name: str, namespace: str, wait_rollout: bool = True) -> None:
    """
    Patch a deployment and wait for rollout.
    """
    now = datetime.now().strftime("%Y%m%d-%H%M%S")
    k8s_patch = {"spec": {"template": {"metadata": {"labels": {"date": now}}}}}
    v1_apps = get_v1apps_api()
    v1_apps.patch_namespaced_deployment(name=name, namespace=namespace, body=k8s_patch)
    if wait_rollout:
        execute(f'kubectl rollout status -n{namespace} deployments/{name}')
