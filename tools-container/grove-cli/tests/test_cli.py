from pathlib import Path
from unittest import mock

from typer.testing import CliRunner

from bin.cli import app

from .utils import TEST_INSTANCE_NAME_A

runner = CliRunner()


@mock.patch("bin.cli.Instance")
def test_new(mock_instance):
    # instance_name is required
    result = runner.invoke(app, ["new"])
    assert result.exit_code > 0
    assert "Error: Missing argument 'INSTANCE_NAME'." in result.stdout

    # create new instance
    result = runner.invoke(app, ["new", TEST_INSTANCE_NAME_A])
    assert result.exit_code == 0
    mock_instance.assert_called_with(TEST_INSTANCE_NAME_A)


@mock.patch("bin.cli.Instance")
def test_webhookpipeline(mock_instance):
    result = runner.invoke(app, ["webhookpipeline"])
    assert result.exit_code > 0

    result = runner.invoke(
        app,
        [
            "webhookpipeline",
            str(Path(__file__).parent / "fixtures" / "github.json"),
        ],
    )
    assert result.exit_code == 0

    result = runner.invoke(
        app,
        [
            "webhookpipeline",
            str(Path(__file__).parent / "fixtures" / "gitlab.json"),
        ],
    )
    assert result.exit_code == 0
