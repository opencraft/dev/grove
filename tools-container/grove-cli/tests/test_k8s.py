"""
Tests for k8s.py.
"""
from unittest.case import TestCase
from unittest.mock import Mock, patch

from grove.k8s import get_v1apps_api, restart_deployment


class TestK8SUtils(TestCase):
    """
    Tests for k8s utilities.
    """

    @patch('grove.k8s.config.load_kube_config')
    @patch('grove.k8s.client.AppsV1Api')
    def test_get_v1apps_api(self, mock_v1apps: Mock, mock_config: Mock):
        """
        Test get_v1apps calls load_kub_config and AppsV1Api.
        """
        _ = get_v1apps_api()
        mock_config.assert_called()
        mock_v1apps.assert_called()

    @patch('grove.k8s.execute')
    @patch('grove.k8s.get_v1apps_api')
    def test_restart_deployment(self, mock_v1apps: Mock, mock_execute: Mock):
        """
        Test patch_deployment calls the patch API and rollout status.
        """
        mock_api = Mock()
        mock_api.patch_namespaced_deployment = Mock()
        mock_v1apps.return_value = mock_api

        restart_deployment('deployment', 'namespace')

        mock_api.patch_namespaced_deployment.assert_called()
        mock_execute.assert_called_once_with(
            'kubectl rollout status -nnamespace deployments/deployment'
        )

    @patch('grove.k8s.execute')
    @patch('grove.k8s.get_v1apps_api')
    def test_restart_deployment_no_wait(self, mock_v1apps: Mock, mock_execute: Mock):
        """
        Test patch_deployment calls the patch API and rollout status.
        """
        mock_api = Mock()
        mock_api.patch_namespaced_deployment = Mock()
        mock_v1apps.return_value = mock_api

        restart_deployment('deployment', 'namespace', False)

        mock_api.patch_namespaced_deployment.assert_called()
        mock_execute.assert_not_called()
