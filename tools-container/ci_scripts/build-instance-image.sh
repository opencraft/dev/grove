#!/usr/bin/env bash
set -eou pipefail

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
grove tutor buildimage $INSTANCE_NAME $IMAGE_NAME \
      --edx-platform-repository="$EDX_PLATFORM_REPOSITORY" \
      --edx-platform-version="$EDX_PLATFORM_VERSION"
exec grove tutor exec $INSTANCE_NAME "images push $IMAGE_NAME"
