# Changelog

All notable changes to this project will be documented in this file.

## [0.18.1] - 2024-12-18

**Bug Fixes**

- Setting AWS S3 Bucket host from terraform to be used in grove deployments ([75d9ef7](https://gitlab.com/opencraft/dev/grove/-/commit/75d9ef72d65fd46914d338c8c973a795f4cfe285))

**Documentation**

- Add note about removing configs for pr sandbox ([b82aab3](https://gitlab.com/opencraft/dev/grove/-/commit/b82aab331f1a71c95e21c95d172ea4df057311ad))

**Features**

- Disable velero node-agent ([b950d20](https://gitlab.com/opencraft/dev/grove/-/commit/b950d2075f1a3fa58348f931c4abbe3208aa87a0))
- Add k8s-cleaner to kubernetes clusters ([a420215](https://gitlab.com/opencraft/dev/grove/-/commit/a42021559ee79a5ccac0df2a8f494c0e116c9ecf))
- RDS storage space alarm ([1a98e5b](https://gitlab.com/opencraft/dev/grove/-/commit/1a98e5b25e3de3c75ea20e557b405ab9ed2afe95))
- Allow setting worker node root volume size ([934f1a2](https://gitlab.com/opencraft/dev/grove/-/commit/934f1a260c5347ca5472d4d87e9d97e17996ea05))

**Miscellaneous Tasks**

- Pr watcher add redwood settings ([ec5dcd1](https://gitlab.com/opencraft/dev/grove/-/commit/ec5dcd1522ce9e85af243b886774dc6cfef617ea))
- Bump Grove tooling ([baac8d1](https://gitlab.com/opencraft/dev/grove/-/commit/baac8d1e8c4d7316c56546c512a1adc58ad2f10d))

**Refactor**

- Remove unused terraform modules ([eb06e27](https://gitlab.com/opencraft/dev/grove/-/commit/eb06e27c7f1c1fdc080717e20a669e976d3ac849))

## [0.16.0] - 2024-03-14

**Miscellaneous Tasks**

- **Breaking:** Bump mongodb to 7 ([ef8275f](https://gitlab.com/opencraft/dev/grove/-/commit/ef8275ff3cb2bedfdb27d6378f09348544625ee4))
MongoDB supports upgrading the clusters one major release at a time. Therefore, you must apply the v6 upgrade with Grove v0.15.0 first.

## [0.15.0] - 2024-03-14

**Miscellaneous Tasks**

- **Breaking:** Bump mongodb to 6 ([0e8a7ab](https://gitlab.com/opencraft/dev/grove/-/commit/0e8a7ab9d6a3a921790279db666efccaa2db7bd8))
MongoDB supports upgrading the clusters one major release at a time. Therefore, you must apply the v5 upgrade with Grove v0.14.0 first.

## [0.14.0] - 2024-03-14

**Bug Fixes**

- New requirements not applied to existing instance and some fixes for pr_watcher ([ca0a256](https://gitlab.com/opencraft/dev/grove/-/commit/ca0a256f133b370b209b9a82b538ae0b10f139ab))

**Documentation**

- Improve pr watcher docs ([ffeaeec](https://gitlab.com/opencraft/dev/grove/-/commit/ffeaeec307015f70afae15a4edd1a03f0f3686c7))

**Features**

- Make pr_watcher success/failure comments more compact ([137ff9f](https://gitlab.com/opencraft/dev/grove/-/commit/137ff9f579e4b4438a98bbcf8998497907311857))

**Miscellaneous Tasks**

- Bump mongodb to 5 ([7dce62e](https://gitlab.com/opencraft/dev/grove/-/commit/7dce62e611f98839f24efd75181c6f021ed4adfd))

## [0.13.0] - 2024-02-15

**Bug Fixes**

- Fix issues with comparing datetimes ([826c929](https://gitlab.com/opencraft/dev/grove/-/commit/826c9297f00f37f3080762091f383a05828e2348))
- Don't add extra https to allowed_cors_origins ([e3c7b41](https://gitlab.com/opencraft/dev/grove/-/commit/e3c7b4122cc3063fe6a4d5fd584091c28a2af8b5))
- Ensure PR body is not None ([06e3216](https://gitlab.com/opencraft/dev/grove/-/commit/06e3216ec271e71dac7e4c6b66dc4ac8ade03687))
- Remove edx image cache ([a1edfdd](https://gitlab.com/opencraft/dev/grove/-/commit/a1edfdd511fd2c31937e639d57228ffed0d17dff))
- Ensure ingress-nginx uses the incoming X-Forwarded-For header ([4d69a29](https://gitlab.com/opencraft/dev/grove/-/commit/4d69a29f31311ac053b4ce2d4791676b1868b88f))
- Tools-container build failing due to pip install failure without venv ([8dbfc48](https://gitlab.com/opencraft/dev/grove/-/commit/8dbfc4840d9727043924baf1f398dd7df1feab53))

**Documentation**

- Add discovery document ([615d4cf](https://gitlab.com/opencraft/dev/grove/-/commit/615d4cf3a9208fb90505935fbefdeeb462817611))
- Add service information for cron jobs ([e9a5482](https://gitlab.com/opencraft/dev/grove/-/commit/e9a5482cae5fc9c88011e7300f419d71661e416f))

**Features**

- Sandbox deprovisioning ([4f23ee5](https://gitlab.com/opencraft/dev/grove/-/commit/4f23ee5fc460ee71d3bb10f965bd39fdd013fa0d))
- Post config file with failure notification ([46d3d4e](https://gitlab.com/opencraft/dev/grove/-/commit/46d3d4ecaf86e3d35d236faeaa27797e3f3302f9))
- Reuse code that generates allowed_cors_origins ([ebcdd45](https://gitlab.com/opencraft/dev/grove/-/commit/ebcdd45a23490afda78e26bceee08afdcb2745af))
- Pass instances configs and allowed_cors_origins to plugin ([1090b19](https://gitlab.com/opencraft/dev/grove/-/commit/1090b1973f0092b057c4e9bfc2562e1a72dbb9de))
- Allow label based sandbox management ([96bae21](https://gitlab.com/opencraft/dev/grove/-/commit/96bae21c8155d164cfab4fd7a43a54e6f9073d1a))
- Upgrade default rds_mysql_version + added  rds_ca_cert_identifier ([298c197](https://gitlab.com/opencraft/dev/grove/-/commit/298c1972133521394973aded7a9024c3564a8be4))
- Remove usage of private.yml file ([8a0753e](https://gitlab.com/opencraft/dev/grove/-/commit/8a0753e7d7dcfa9e2bba5227ba68ae21de398c97))

**Miscellaneous Tasks**

- Bump tools-container image version ([739f6e8](https://gitlab.com/opencraft/dev/grove/-/commit/739f6e8d1ee422971d277533cf65340e072b0e95))
- Bump pr_watcher version ([7814e1c](https://gitlab.com/opencraft/dev/grove/-/commit/7814e1c69f9f531154ca3dc6fb38e95ddb70325b))
- Bump grove requirements and dependencies ([3336be8](https://gitlab.com/opencraft/dev/grove/-/commit/3336be8f5813dcb207fff326128708cda01e7cb6))

**Styling**

- Terraform fmt ([0107d55](https://gitlab.com/opencraft/dev/grove/-/commit/0107d559fd1ae802b7c5e39daf4ec67245ad077c))

## [0.12.0] - 2023-08-30

**Bug Fixes**

- Cache-from/cache-to need 'ref=' prefix ([6e285ef](https://gitlab.com/opencraft/dev/grove/-/commit/6e285ef5fccbf5f5c49f59313217bd7b8b38d43f))
- PyYAML issues with Cython ([251c0ed](https://gitlab.com/opencraft/dev/grove/-/commit/251c0ed76c408a365f10c6fb3a30a25f7b2280c9))
- Conditionally set source_addresses for do provider inbound_rule ([f1aab20](https://gitlab.com/opencraft/dev/grove/-/commit/f1aab20c07234182777c1d85131e74e3bec0839d))
- Improve experience with MacOS or ARM hosts, update docs ([8e375f2](https://gitlab.com/opencraft/dev/grove/-/commit/8e375f2af0141ca98a8eb0ea8b2f995ddfa9e6d8))
- Permission issues when UID != 1000 ([2f2536a](https://gitlab.com/opencraft/dev/grove/-/commit/2f2536a18273885059285a116c37cdac133803d1))
- Permission issues when UID != 1000 ([2abc93d](https://gitlab.com/opencraft/dev/grove/-/commit/2abc93de996884534337dae40eac3bb450923a0a))
- Do not print command when generating kubeconfig file ([4404cfa](https://gitlab.com/opencraft/dev/grove/-/commit/4404cfae4646edbe8b6063c140790791ff9914b4))

**Documentation**

- Add custom database user guides ([c5ffdcd](https://gitlab.com/opencraft/dev/grove/-/commit/c5ffdcd8e792ee106d73d51adeef9b5dda77695f))
- Fix variable name ([a7366d7](https://gitlab.com/opencraft/dev/grove/-/commit/a7366d76f72ae2d4e79dec610b4edb699798a753))
- Update multi-domain setup docs ([ea121dc](https://gitlab.com/opencraft/dev/grove/-/commit/ea121dce3619beb25ff7b1c295d534ac9721c448))
- Describe how to fully update an instance ([4b6d1d6](https://gitlab.com/opencraft/dev/grove/-/commit/4b6d1d622dec63212bd25fa2390ad425145ca3ab))
- Add targeted mode documentation ([074acd5](https://gitlab.com/opencraft/dev/grove/-/commit/074acd5d49077df5590a2acb28532eed3355be48))
- Update themes configuration settings ([cdd8217](https://gitlab.com/opencraft/dev/grove/-/commit/cdd82178f75d1396e3b410df055ae636df40d033))
- Update themes configuration settings ([e748c0e](https://gitlab.com/opencraft/dev/grove/-/commit/e748c0eca2238b4d42d4e8ef06d8cdb83cbde79a))
- Fix typo in working-locally userguide ([cb5ecad](https://gitlab.com/opencraft/dev/grove/-/commit/cb5ecad0bba2cd7e005c0899049a261c70d42c8c))

**Features**

- Add support for image build on tutor 16 ([54ed4ae](https://gitlab.com/opencraft/dev/grove/-/commit/54ed4ae87fbbd7f60ec892615b6e4560e1cc9b2c))
- Speed up loading of output data from Terraform state ([30ff766](https://gitlab.com/opencraft/dev/grove/-/commit/30ff766fef44f0aaa764d0179b8d52680b8d7a13))
- Add scripts dir support ([1983c48](https://gitlab.com/opencraft/dev/grove/-/commit/1983c4850a986952525f49f1188cda10ad1054f7))
- Make PR sandboxes configurable ([6fea3fb](https://gitlab.com/opencraft/dev/grove/-/commit/6fea3fb01315878badddeafdd402e46cb5b2f942))
- Allow specifying scss overrides via dictionaries ([4d969fc](https://gitlab.com/opencraft/dev/grove/-/commit/4d969fc9c2ea6394b6a25ade87fb7b5f5b5432cf))
- Expose ES certificate in PEM form ([f927201](https://gitlab.com/opencraft/dev/grove/-/commit/f92720187161ef7e66ba5c3399dc4304b1449e45))

**Miscellaneous Tasks**

- Bump up tools-container image version ([d4b26f7](https://gitlab.com/opencraft/dev/grove/-/commit/d4b26f7dd2e88705c257324d088423e1c7bccdef))
- Bump provider and chart versions ([73ec65e](https://gitlab.com/opencraft/dev/grove/-/commit/73ec65e21ea12391db8b15d1650bd16f7f5105f8))
- Update tooling and dependencies ([ef6a309](https://gitlab.com/opencraft/dev/grove/-/commit/ef6a3091294057aabac1cf47b5157b29035a7a5b))

**Refactor**

- Update default mongodb version ([65a55f3](https://gitlab.com/opencraft/dev/grove/-/commit/65a55f33843cfabfe761efda56348d421cc83683))
- Print commands before running them ([afe300b](https://gitlab.com/opencraft/dev/grove/-/commit/afe300be34c8d510718220a0c29d013658fe7a35))

**Styling**

- Run `make format` ([6923721](https://gitlab.com/opencraft/dev/grove/-/commit/692372164e3649a895d66eab0e96de8afe4b1868))

**Testing**

- Add test for generating scss overrides ([298f58a](https://gitlab.com/opencraft/dev/grove/-/commit/298f58aa2e1e1fcd174eaa92d6ba6f43913ac78d))

**Revert**

- "fix: permission issues when UID != 1000" ([cbd24a3](https://gitlab.com/opencraft/dev/grove/-/commit/cbd24a30e1c4b41bb9a35ac9a9c8341cf99d040a))
- "Merge branch 'mtyaka/SE-5946-multi-az-nat' into 'main'" ([c2170b8](https://gitlab.com/opencraft/dev/grove/-/commit/c2170b802c3d8648fd39f2f3ac8615954584f290))

## [0.11.0] - 2023-06-10

**Features**

- Use default for cors if not defined ([bcdc186](https://gitlab.com/opencraft/dev/grove/-/commit/bcdc186d994b3287d1a44b0464e0343493b76a6d))
- Fluent-bit log forwarding for cloudwatch ([18482c6](https://gitlab.com/opencraft/dev/grove/-/commit/18482c6330ff367906a3f262b517b3de5f21a3c8))
- Terraform integration for New Relic ([e598c21](https://gitlab.com/opencraft/dev/grove/-/commit/e598c21a9373b06801bb3d17d347664faa3fd0ee))
- Use s3 storage for static files ([7fb9146](https://gitlab.com/opencraft/dev/grove/-/commit/7fb91466846c94a99c78825f883088e5c9258386))

## [0.10.0] - 2023-06-07

**Bug Fixes**

- Hardcode vpc version, otherwise it uses latest aws provider ([906d78e](https://gitlab.com/opencraft/dev/grove/-/commit/906d78e129948ac0e91c61946fe697f86746d891))
- Ensure s3 url has no bucket for velero in AWS ([fc531e7](https://gitlab.com/opencraft/dev/grove/-/commit/fc531e7937b158ab658c6bc6c668bc3d1765d736))

**Documentation**

- Mutli-tenant setup with eox-tenant ([ec974ca](https://gitlab.com/opencraft/dev/grove/-/commit/ec974ca02703cce342b70fabe3c23e61ebae24a1))

**Features**

- Cloudfront cdn support for aws provider ([0c3577b](https://gitlab.com/opencraft/dev/grove/-/commit/0c3577b8f102f4687e1f53387e8bc3e8d9942c44))
- Add retention policies for OpenSearch ([f26ad10](https://gitlab.com/opencraft/dev/grove/-/commit/f26ad105c50f150efe2b5ba1b105f8efa9115c41))
- Add Velero backups ([e89586b](https://gitlab.com/opencraft/dev/grove/-/commit/e89586bf895e89cb9299183d94798efde94f42b1))

## [0.9.0] - 2023-05-18

**Bug Fixes**

- Dismiss AWS S3 ACL errors ([5c72891](https://gitlab.com/opencraft/dev/grove/-/commit/5c72891faeb8d3432122b89a569dadfe5a3ce2cd))
- Ensure namespace deletion not timing out ([8d61253](https://gitlab.com/opencraft/dev/grove/-/commit/8d612534cf49e2e51fca76cd1c04ef93bf53f076))
- Add forced bucket deletion ([f942f34](https://gitlab.com/opencraft/dev/grove/-/commit/f942f34f50e9ae6aaa8609c3ac279affd78361d0))
- Bump tools container version ([941d3b2](https://gitlab.com/opencraft/dev/grove/-/commit/941d3b22bcaf3506f4e240e5337f642a21293ae8))
- Don't block port 8001 unless it's being forwarded ([6919c5d](https://gitlab.com/opencraft/dev/grove/-/commit/6919c5d15165b72c9e94fdd610d50c46a840e8fc))

**Documentation**

- Update documentation for shared ES after load tests ([fb6a6b2](https://gitlab.com/opencraft/dev/grove/-/commit/fb6a6b2f076d8487c2b8eaf1e4aa1a929004a151))
- Fix TF variable name ([c6edd1b](https://gitlab.com/opencraft/dev/grove/-/commit/c6edd1bedd004be6fe71238fa5b44449c44ff18c))
- Added docs for GROVE_REDIRECTS setting ([d4ed5b9](https://gitlab.com/opencraft/dev/grove/-/commit/d4ed5b911a160ce5bcbe9c417d1c9f80fbf30481))

**Features**

- Add bucket CORS list ([e5f5077](https://gitlab.com/opencraft/dev/grove/-/commit/e5f507739522edc967f1e296e5e890d0c940a959))
- Add suport for tutor 15 ([915e5a0](https://gitlab.com/opencraft/dev/grove/-/commit/915e5a0af0a1c07e48d55988da4a0efb557a3e78))
- Configurable docker build parallelism ([9156ba8](https://gitlab.com/opencraft/dev/grove/-/commit/9156ba8a3e9573ff790076553af66e5efcef5be4))
- Implement ami filtering ([5872462](https://gitlab.com/opencraft/dev/grove/-/commit/587246240c7339c7bf50a077cdd2887b6801d223))
- Ensure required instance host config set ([5f8ba37](https://gitlab.com/opencraft/dev/grove/-/commit/5f8ba373b47b6fce1b1b2ecc3f6e43192ab121da))
- Allow custom user and database names ([4addb93](https://gitlab.com/opencraft/dev/grove/-/commit/4addb93eb2f3244803f846d4e350cc669df48836))
- Allow multiple themes ([9bb3321](https://gitlab.com/opencraft/dev/grove/-/commit/9bb3321cd4ab18fde399a1588f72aaca2da9fd1a))
- Configurable resources for OpenSearch and NGINX ([b4ea28f](https://gitlab.com/opencraft/dev/grove/-/commit/b4ea28fef3f9fb76818b1042ee4124d185548c49))

**Miscellaneous Tasks**

- Bump CI vars ([2155186](https://gitlab.com/opencraft/dev/grove/-/commit/215518634162d28fb34737f3327b29d255f7670f))

**Refactor**

- Update OpenFAAS PR watcher cron interval ([9ad1723](https://gitlab.com/opencraft/dev/grove/-/commit/9ad17234302df95cf25ce3faa7c81c65e927dc02))

**Build**

- Add default empty terraformrc ([0d0198d](https://gitlab.com/opencraft/dev/grove/-/commit/0d0198d773bb73783ea5a5ce36fbb25d2bee322b))

## [0.8.1] - 2023-04-04

**Bug Fixes**

- Ensure new isntance creation works ([ac0cc66](https://gitlab.com/opencraft/dev/grove/-/commit/ac0cc66e3b37f81f75767108c9c6c1729f995a91))

**Documentation**

- Add notes on increasing disk size ([ab9db27](https://gitlab.com/opencraft/dev/grove/-/commit/ab9db277ea6c3566a5fc50bce262f1e26239f99d))

## [0.8.0] - 2023-04-03

**Bug Fixes**

- Add hostname annotation to DO load balancer ([58651e9](https://gitlab.com/opencraft/dev/grove/-/commit/58651e9462c1b833fd30795fdac8536578de0775))
- Ensure git safe-dir is set ([634d6d5](https://gitlab.com/opencraft/dev/grove/-/commit/634d6d5b055b92ed504ddda015df2304df7f2fc4))

**Documentation**

- Setting up edxapp feature flags ([47825eb](https://gitlab.com/opencraft/dev/grove/-/commit/47825eb28e1abe8b6eb27bdbb19a52a27f35d31c))
- Add ocim integration docs ([49c8f8c](https://gitlab.com/opencraft/dev/grove/-/commit/49c8f8c2eab209087563f4b56c5087a0d0811193))

**Features**

- Extensible terraform config ([b2e8473](https://gitlab.com/opencraft/dev/grove/-/commit/b2e8473bdc82c56a8e9892f7e49c16b24714eff6))
- Add dedicated grove user ([6c69e31](https://gitlab.com/opencraft/dev/grove/-/commit/6c69e314a373ed1ad728f6ab1089ed036d0e8149))
- Add dedicated grove user ([3202039](https://gitlab.com/opencraft/dev/grove/-/commit/32020392f22c88e80c8e3d1cb2ab61b7f64cbea4))
- Switch docker builds to user buildx instead of native ([83e0e6d](https://gitlab.com/opencraft/dev/grove/-/commit/83e0e6d852a60a403b21fa0c7225060a13ccbbe7))
- Private repo support and docker updates ([a9c759e](https://gitlab.com/opencraft/dev/grove/-/commit/a9c759ea312e3be4aa80a6d728994ac5d4819ec6))

## [0.7.0] - 2023-02-28

**Bug Fixes**

- Don't fail if pod logs are unavailable ([b1b7c71](https://gitlab.com/opencraft/dev/grove/-/commit/b1b7c71d96d27aabe61719991c14871035b97b52))
- Runners that broken due to docker:dind upgrade ([ef27187](https://gitlab.com/opencraft/dev/grove/-/commit/ef271877156fe1e28d1548a2a90781b2de0d9a8a))
- Path to tutor executable for settheme command ([a663a5f](https://gitlab.com/opencraft/dev/grove/-/commit/a663a5f6c92adc28e2c17c9f315159e05aec839a))
- Add labels necessary for autoscaling ([d898f52](https://gitlab.com/opencraft/dev/grove/-/commit/d898f529661e2e2be660c8fa79e4d49422216155))

**Documentation**

- Updated documentation to reflect latest code changes ([933f2e4](https://gitlab.com/opencraft/dev/grove/-/commit/933f2e4a9accb9f6c342293b074057db1f9e26b7))
- Added documentation on multi-domain setups ([7b8e6e7](https://gitlab.com/opencraft/dev/grove/-/commit/7b8e6e74dfefd217c4fe2b836edd6eb48e928649))

**Features**

- Shared Elasticsearch clusters can be used ([56746b0](https://gitlab.com/opencraft/dev/grove/-/commit/56746b0dd48d0c4481ffdeb998d249cc319c9a6e))
- Add helm wrapper ([2029980](https://gitlab.com/opencraft/dev/grove/-/commit/202998032308d5704e8f853a92b89175ddfa59ba))
- Add helm wrapper ([efaa61a](https://gitlab.com/opencraft/dev/grove/-/commit/efaa61a6ae25f6001c0ec973dace613ad5deced5))
- S3 support for fluent-bit logs ([d16e67a](https://gitlab.com/opencraft/dev/grove/-/commit/d16e67a857fa176d331e5f3a4bd2a501a57910b9))
- Enable proxy protocol on ingress controller ([c8db1e1](https://gitlab.com/opencraft/dev/grove/-/commit/c8db1e1e10b8e200c6e31a6f7f41948dae3748f8))

**Miscellaneous Tasks**

- Child pipelines no longer need to be cancelled explicitly ([65121d2](https://gitlab.com/opencraft/dev/grove/-/commit/65121d2c0b7dc93ce06cf12bb37d1c47e49b55ac))
- Bump tools-container version to 0.2.3 ([c0e7175](https://gitlab.com/opencraft/dev/grove/-/commit/c0e7175359c625db054de716dc9ffdf3db25149c))
- Hardcode docker:dind version so that upgrades are smooth ([4af3b0d](https://gitlab.com/opencraft/dev/grove/-/commit/4af3b0d50e0a9e77e08a17d713af587fdff54095))
- Bump tools-container version ([8490a7c](https://gitlab.com/opencraft/dev/grove/-/commit/8490a7cbe94c69ad7e30e6c6994de185a2d076d8))
- Bump tools-container version ([1bf6ef3](https://gitlab.com/opencraft/dev/grove/-/commit/1bf6ef325d9a8530bd77ab728ec9e4ef68156eb5))

## [0.6.1] - 2022-12-24

**Bug Fixes**

- Revert GITLAB_TERRAFORM_IMAGES_VERSION ([7d3b2e8](https://gitlab.com/opencraft/dev/grove/-/commit/7d3b2e8e561217be604dcc69f4edd97d6cfc2751))

## [0.6.0] - 2022-12-23

**Bug Fixes**

- Update Grove to work with new forum config params ([a8ac4db](https://gitlab.com/opencraft/dev/grove/-/commit/a8ac4db41cf0bbbaa8f96c080237e3ab02e95300))
- Ensure RDS creation does not prevent destroy ([e0bef9e](https://gitlab.com/opencraft/dev/grove/-/commit/e0bef9ec07723ee762a377c649363c2862a0c5e4))
- Multiple rds snapshot creation ([2b29efb](https://gitlab.com/opencraft/dev/grove/-/commit/2b29efbb7f8b2154825b828a07c90a6d4b6c065c))
- Install py3 requirments as part of test ([b8af83a](https://gitlab.com/opencraft/dev/grove/-/commit/b8af83a6a987bfd54998705f84cf55d289544e8b))

**Documentation**

- Discovery on using virtual environments for tutor ([41e6704](https://gitlab.com/opencraft/dev/grove/-/commit/41e6704e6beff3a6f5a86c90bc86cc7d483064ea))

**Features**

- Pr watcher for Grove ([544aa89](https://gitlab.com/opencraft/dev/grove/-/commit/544aa8946e21a3be93d50add938685e73cd0fb7a))
- Allow infratest destroy ([e17ce87](https://gitlab.com/opencraft/dev/grove/-/commit/e17ce8770081b21747f12c642cee748857399f9f))
- Virtual environment support for instances ([6141b0f](https://gitlab.com/opencraft/dev/grove/-/commit/6141b0f49e8b62983f5fada77a2b37044bbdda05))
- Add support for aws regions and amis ([61dd589](https://gitlab.com/opencraft/dev/grove/-/commit/61dd5897d8934b4c4a36f23c6fe7393a9d2734a8))

**Miscellaneous Tasks**

- Pin digitalocean cluster version ([b2b7c52](https://gitlab.com/opencraft/dev/grove/-/commit/b2b7c527efabfca4909ebfab4b551e8d648d229f))
- Bump tools-container version ([d039e64](https://gitlab.com/opencraft/dev/grove/-/commit/d039e64e2f1b1206f7ccb53d03f6bdadd899d18c))
- Upgrade AWS to 1.24, DO to v1.25 ([59add48](https://gitlab.com/opencraft/dev/grove/-/commit/59add4851ecddc9486a824484578cec79971ac29))
- Revert tutor dependency ([6d2cbff](https://gitlab.com/opencraft/dev/grove/-/commit/6d2cbff5bd2d67b9573413e92f1efd3e5d2a258f))
- Bump tools-container ci vars ([645bf28](https://gitlab.com/opencraft/dev/grove/-/commit/645bf287ef2ac352857f70867a373a0591dbed50))

**Refactor**

- Call infra tests on direct request ([349d25b](https://gitlab.com/opencraft/dev/grove/-/commit/349d25b4188c04045d2bd2e32a4c99eee5baf745))
- Call infra tests on direct request ([d77261f](https://gitlab.com/opencraft/dev/grove/-/commit/d77261f7a67e454631e6f700f316fe4fdd47e936))

## [0.5.2] - 2022-11-18

**Miscellaneous Tasks**

- Bump eks version to 1.24 ([0b1ce43](https://gitlab.com/opencraft/dev/grove/-/commit/0b1ce43dc1fdd3e4394712145d83839f63c779be))

## [0.5.1] - 2022-11-18

**Miscellaneous Tasks**

- Bump eks version to 1.23 ([360d296](https://gitlab.com/opencraft/dev/grove/-/commit/360d29617746c897fdf2fe928d856b2a9d394553))

## [0.5.0] - 2022-11-18

**Bug Fixes**

- Aws issues that arose from going through the checklist ([5879e38](https://gitlab.com/opencraft/dev/grove/-/commit/5879e38b64956cb8bc52f13d693864a22b911dc4))
- DO errors caused by AWS changes ([0e74a82](https://gitlab.com/opencraft/dev/grove/-/commit/0e74a82d38000e355ec4f4d00157c789ebac9d28))

**Documentation**

- Ocim V3 discovery ([af4ba95](https://gitlab.com/opencraft/dev/grove/-/commit/af4ba9558ffbe6fd24f9a83026f5a5b4f8007632))
- Add grove intro docs ([96e0ae6](https://gitlab.com/opencraft/dev/grove/-/commit/96e0ae639ca498968a43e5c8857c0fa9a13f0c86))

**Features**

- Monthly usage reports per namespace ([15ff74c](https://gitlab.com/opencraft/dev/grove/-/commit/15ff74cb1fd7c01f341b954690e6b5ca7f4ce3cc))
- Grove porcelain for pipelines ([0523f90](https://gitlab.com/opencraft/dev/grove/-/commit/0523f90890fa4492b0ebcbaebebfb842538367d7))
- Implement waffle flags ([5ab5de9](https://gitlab.com/opencraft/dev/grove/-/commit/5ab5de9afd37beea1c37c8c884dc10750fcf4480))
- Implement waffle flags ([678d1b2](https://gitlab.com/opencraft/dev/grove/-/commit/678d1b2b28535f6f9c7fc470ca999da69a90b83f))

**Miscellaneous Tasks**

- Added rule for infra test ([0c44009](https://gitlab.com/opencraft/dev/grove/-/commit/0c440098d8430d23f8f9170c3e7d70ccb7ec8e27))
- Bump terraform provider versions ([a0ad54a](https://gitlab.com/opencraft/dev/grove/-/commit/a0ad54a595dcf1061613133571bd5480cf266f59))
- Bump terraform, kubectl, gitlab tf versions ([512dcbb](https://gitlab.com/opencraft/dev/grove/-/commit/512dcbb77a262bb749237e3b8c045f9ca0bed5eb))
- Bump requirements ([253ffbc](https://gitlab.com/opencraft/dev/grove/-/commit/253ffbcf21e919c4403afcda1caaa46c2d0be3c1))
- Bump doc requirements ([eef0bb0](https://gitlab.com/opencraft/dev/grove/-/commit/eef0bb0579d1237beb801f32f933be6042efca6a))
- Revert accidental commits ([b9a3b78](https://gitlab.com/opencraft/dev/grove/-/commit/b9a3b78b3835fdfc9dbb17ad09f9a6b9a28c511b))
- Bump tools-container version ([8644aae](https://gitlab.com/opencraft/dev/grove/-/commit/8644aae11b90f888ad71550bc4ef5924b8e1285e))
- Bump tools-container version ([2e2969d](https://gitlab.com/opencraft/dev/grove/-/commit/2e2969da6cdc6e4a7baecc71220da8f46d2969e0))
- Bump eks version to 1.22 ([b957bc1](https://gitlab.com/opencraft/dev/grove/-/commit/b957bc1ba579a2d037686c2380f6f3f05d966286))
- Bump eks version to 1.22 ([7572284](https://gitlab.com/opencraft/dev/grove/-/commit/75722847ca708439f296e3fbf2ba329980ed91bc))

**Refactor**

- Remove hardcoded node type validation ([e519b38](https://gitlab.com/opencraft/dev/grove/-/commit/e519b38453a6fdf7582dbfaab93c431e12a48d2a))
- Remove hardcoded node type validation ([92e82df](https://gitlab.com/opencraft/dev/grove/-/commit/92e82dfb7fc6ed08b55ab90601245130118c8cbf))

## [0.4.0] - 2022-10-03

**Bug Fixes**

- Default "" as var.vpc_ip_range (optional tf arg) ([435470f](https://gitlab.com/opencraft/dev/grove/-/commit/435470fb49c1b22e280bf83ac91b90f99d619fb4))
- Default "" as var.vpc_ip_range (optional tf arg) ([472eb9c](https://gitlab.com/opencraft/dev/grove/-/commit/472eb9c2dcc721443ad6eae035dd12f581405be8))

**Features**

- Add support for tutor-forum plugin ([d4132a4](https://gitlab.com/opencraft/dev/grove/-/commit/d4132a454ff54212c602bc2e392dc1ff21af1eba))

**Miscellaneous Tasks**

- OpenFaas function to get metrics from Kubernetes ([857fe4e](https://gitlab.com/opencraft/dev/grove/-/commit/857fe4e5ded931e7c2f55d7d44e1ffe092c4dab4))

**Testing**

- Add pipelines to test cluster creation ([ab02a09](https://gitlab.com/opencraft/dev/grove/-/commit/ab02a097d11c8d982d771e85325946fb509f6676))

## [0.5.1] - 2022-11-18

**Miscellaneous Tasks**

- Bump eks version to 1.23 ([360d296](https://gitlab.com/opencraft/dev/grove/-/commit/360d29617746c897fdf2fe928d856b2a9d394553))

## [0.5.0] - 2022-11-18

**Bug Fixes**

- Aws issues that arose from going through the checklist ([5879e38](https://gitlab.com/opencraft/dev/grove/-/commit/5879e38b64956cb8bc52f13d693864a22b911dc4))
- DO errors caused by AWS changes ([0e74a82](https://gitlab.com/opencraft/dev/grove/-/commit/0e74a82d38000e355ec4f4d00157c789ebac9d28))

**Documentation**

- Ocim V3 discovery ([af4ba95](https://gitlab.com/opencraft/dev/grove/-/commit/af4ba9558ffbe6fd24f9a83026f5a5b4f8007632))
- Add grove intro docs ([96e0ae6](https://gitlab.com/opencraft/dev/grove/-/commit/96e0ae639ca498968a43e5c8857c0fa9a13f0c86))

**Features**

- Monthly usage reports per namespace ([15ff74c](https://gitlab.com/opencraft/dev/grove/-/commit/15ff74cb1fd7c01f341b954690e6b5ca7f4ce3cc))
- Grove porcelain for pipelines ([0523f90](https://gitlab.com/opencraft/dev/grove/-/commit/0523f90890fa4492b0ebcbaebebfb842538367d7))
- Implement waffle flags ([5ab5de9](https://gitlab.com/opencraft/dev/grove/-/commit/5ab5de9afd37beea1c37c8c884dc10750fcf4480))
- Implement waffle flags ([678d1b2](https://gitlab.com/opencraft/dev/grove/-/commit/678d1b2b28535f6f9c7fc470ca999da69a90b83f))

**Miscellaneous Tasks**

- Added rule for infra test ([0c44009](https://gitlab.com/opencraft/dev/grove/-/commit/0c440098d8430d23f8f9170c3e7d70ccb7ec8e27))
- Bump terraform provider versions ([a0ad54a](https://gitlab.com/opencraft/dev/grove/-/commit/a0ad54a595dcf1061613133571bd5480cf266f59))
- Bump terraform, kubectl, gitlab tf versions ([512dcbb](https://gitlab.com/opencraft/dev/grove/-/commit/512dcbb77a262bb749237e3b8c045f9ca0bed5eb))
- Bump requirements ([253ffbc](https://gitlab.com/opencraft/dev/grove/-/commit/253ffbcf21e919c4403afcda1caaa46c2d0be3c1))
- Bump doc requirements ([eef0bb0](https://gitlab.com/opencraft/dev/grove/-/commit/eef0bb0579d1237beb801f32f933be6042efca6a))
- Revert accidental commits ([b9a3b78](https://gitlab.com/opencraft/dev/grove/-/commit/b9a3b78b3835fdfc9dbb17ad09f9a6b9a28c511b))
- Bump tools-container version ([8644aae](https://gitlab.com/opencraft/dev/grove/-/commit/8644aae11b90f888ad71550bc4ef5924b8e1285e))
- Bump tools-container version ([2e2969d](https://gitlab.com/opencraft/dev/grove/-/commit/2e2969da6cdc6e4a7baecc71220da8f46d2969e0))
- Bump eks version to 1.22 ([b957bc1](https://gitlab.com/opencraft/dev/grove/-/commit/b957bc1ba579a2d037686c2380f6f3f05d966286))
- Bump eks version to 1.22 ([7572284](https://gitlab.com/opencraft/dev/grove/-/commit/75722847ca708439f296e3fbf2ba329980ed91bc))

**Refactor**

- Remove hardcoded node type validation ([e519b38](https://gitlab.com/opencraft/dev/grove/-/commit/e519b38453a6fdf7582dbfaab93c431e12a48d2a))
- Remove hardcoded node type validation ([92e82df](https://gitlab.com/opencraft/dev/grove/-/commit/92e82dfb7fc6ed08b55ab90601245130118c8cbf))

## [0.4.0] - 2022-10-03

**Bug Fixes**

- Default "" as var.vpc_ip_range (optional tf arg) ([435470f](https://gitlab.com/opencraft/dev/grove/-/commit/435470fb49c1b22e280bf83ac91b90f99d619fb4))
- Default "" as var.vpc_ip_range (optional tf arg) ([472eb9c](https://gitlab.com/opencraft/dev/grove/-/commit/472eb9c2dcc721443ad6eae035dd12f581405be8))

**Features**

- Add support for tutor-forum plugin ([d4132a4](https://gitlab.com/opencraft/dev/grove/-/commit/d4132a454ff54212c602bc2e392dc1ff21af1eba))

**Miscellaneous Tasks**

- OpenFaas function to get metrics from Kubernetes ([857fe4e](https://gitlab.com/opencraft/dev/grove/-/commit/857fe4e5ded931e7c2f55d7d44e1ffe092c4dab4))

**Testing**

- Add pipelines to test cluster creation ([ab02a09](https://gitlab.com/opencraft/dev/grove/-/commit/ab02a097d11c8d982d771e85325946fb509f6676))

## [0.5.0] - 2022-11-18

**Bug Fixes**

- Aws issues that arose from going through the checklist ([5879e38](https://gitlab.com/opencraft/dev/grove/-/commit/5879e38b64956cb8bc52f13d693864a22b911dc4))
- DO errors caused by AWS changes ([0e74a82](https://gitlab.com/opencraft/dev/grove/-/commit/0e74a82d38000e355ec4f4d00157c789ebac9d28))

**Documentation**

- Ocim V3 discovery ([af4ba95](https://gitlab.com/opencraft/dev/grove/-/commit/af4ba9558ffbe6fd24f9a83026f5a5b4f8007632))
- Add grove intro docs ([96e0ae6](https://gitlab.com/opencraft/dev/grove/-/commit/96e0ae639ca498968a43e5c8857c0fa9a13f0c86))

**Features**

- Monthly usage reports per namespace ([15ff74c](https://gitlab.com/opencraft/dev/grove/-/commit/15ff74cb1fd7c01f341b954690e6b5ca7f4ce3cc))
- Grove porcelain for pipelines ([0523f90](https://gitlab.com/opencraft/dev/grove/-/commit/0523f90890fa4492b0ebcbaebebfb842538367d7))
- Implement waffle flags ([5ab5de9](https://gitlab.com/opencraft/dev/grove/-/commit/5ab5de9afd37beea1c37c8c884dc10750fcf4480))
- Implement waffle flags ([678d1b2](https://gitlab.com/opencraft/dev/grove/-/commit/678d1b2b28535f6f9c7fc470ca999da69a90b83f))

**Miscellaneous Tasks**

- Added rule for infra test ([0c44009](https://gitlab.com/opencraft/dev/grove/-/commit/0c440098d8430d23f8f9170c3e7d70ccb7ec8e27))
- Bump terraform provider versions ([a0ad54a](https://gitlab.com/opencraft/dev/grove/-/commit/a0ad54a595dcf1061613133571bd5480cf266f59))
- Bump terraform, kubectl, gitlab tf versions ([512dcbb](https://gitlab.com/opencraft/dev/grove/-/commit/512dcbb77a262bb749237e3b8c045f9ca0bed5eb))
- Bump requirements ([253ffbc](https://gitlab.com/opencraft/dev/grove/-/commit/253ffbcf21e919c4403afcda1caaa46c2d0be3c1))
- Bump doc requirements ([eef0bb0](https://gitlab.com/opencraft/dev/grove/-/commit/eef0bb0579d1237beb801f32f933be6042efca6a))
- Revert accidental commits ([b9a3b78](https://gitlab.com/opencraft/dev/grove/-/commit/b9a3b78b3835fdfc9dbb17ad09f9a6b9a28c511b))
- Bump tools-container version ([8644aae](https://gitlab.com/opencraft/dev/grove/-/commit/8644aae11b90f888ad71550bc4ef5924b8e1285e))
- Bump tools-container version ([2e2969d](https://gitlab.com/opencraft/dev/grove/-/commit/2e2969da6cdc6e4a7baecc71220da8f46d2969e0))
- Bump eks version to 1.22 ([b957bc1](https://gitlab.com/opencraft/dev/grove/-/commit/b957bc1ba579a2d037686c2380f6f3f05d966286))
- Bump eks version to 1.22 ([7572284](https://gitlab.com/opencraft/dev/grove/-/commit/75722847ca708439f296e3fbf2ba329980ed91bc))

**Refactor**

- Remove hardcoded node type validation ([e519b38](https://gitlab.com/opencraft/dev/grove/-/commit/e519b38453a6fdf7582dbfaab93c431e12a48d2a))
- Remove hardcoded node type validation ([92e82df](https://gitlab.com/opencraft/dev/grove/-/commit/92e82dfb7fc6ed08b55ab90601245130118c8cbf))

## [0.4.0] - 2022-10-03

**Bug Fixes**

- Default "" as var.vpc_ip_range (optional tf arg) ([435470f](https://gitlab.com/opencraft/dev/grove/-/commit/435470fb49c1b22e280bf83ac91b90f99d619fb4))
- Default "" as var.vpc_ip_range (optional tf arg) ([472eb9c](https://gitlab.com/opencraft/dev/grove/-/commit/472eb9c2dcc721443ad6eae035dd12f581405be8))

**Features**

- Add support for tutor-forum plugin ([d4132a4](https://gitlab.com/opencraft/dev/grove/-/commit/d4132a454ff54212c602bc2e392dc1ff21af1eba))

**Miscellaneous Tasks**

- OpenFaas function to get metrics from Kubernetes ([857fe4e](https://gitlab.com/opencraft/dev/grove/-/commit/857fe4e5ded931e7c2f55d7d44e1ffe092c4dab4))

**Testing**

- Add pipelines to test cluster creation ([ab02a09](https://gitlab.com/opencraft/dev/grove/-/commit/ab02a097d11c8d982d771e85325946fb509f6676))

## [0.4] - 2022-10-03

**Bug Fixes**

- Default "" as var.vpc_ip_range (optional tf arg) ([435470f](https://gitlab.com/opencraft/dev/grove/-/commit/435470fb49c1b22e280bf83ac91b90f99d619fb4))
- Default "" as var.vpc_ip_range (optional tf arg) ([472eb9c](https://gitlab.com/opencraft/dev/grove/-/commit/472eb9c2dcc721443ad6eae035dd12f581405be8))

**Features**

- Add support for tutor-forum plugin ([d4132a4](https://gitlab.com/opencraft/dev/grove/-/commit/d4132a454ff54212c602bc2e392dc1ff21af1eba))

**Miscellaneous Tasks**

- OpenFaas function to get metrics from Kubernetes ([857fe4e](https://gitlab.com/opencraft/dev/grove/-/commit/857fe4e5ded931e7c2f55d7d44e1ffe092c4dab4))

**Testing**

- Add pipelines to test cluster creation ([ab02a09](https://gitlab.com/opencraft/dev/grove/-/commit/ab02a097d11c8d982d771e85325946fb509f6676))

## [0.3.0] - 2022-08-29

**Bug Fixes**

- New relic code works with urls containing multiple dashes ([918b3db](https://gitlab.com/opencraft/dev/grove/-/commit/918b3dbc4b30fc53ac8c4cfcd8e4e068925c17b4))
- Simplify alert manager configuration to only require receivers ([1622c69](https://gitlab.com/opencraft/dev/grove/-/commit/1622c69aa83ba766a8ed0f7c7957e882ca4d9657))
- Use nerdgraph for fetching new relic monitors ([693175c](https://gitlab.com/opencraft/dev/grove/-/commit/693175c067b8f8aa6320bcdd6d33df7b99d63d70))
- Use nerdgraph for fetching new relic monitors ([eb7df1e](https://gitlab.com/opencraft/dev/grove/-/commit/eb7df1e5c9ea7a584e5e805b45195ef7383b74a1))

**Documentation**

- Add disaster recovery doc ([04e8450](https://gitlab.com/opencraft/dev/grove/-/commit/04e845095d2e8ace1f255c6258946c65d3c4ef38))
- Add note about credentials ([8f7b5d1](https://gitlab.com/opencraft/dev/grove/-/commit/8f7b5d1628720087b842815353f6962af69e1cc5))
- Add note about credentials ([382e8a0](https://gitlab.com/opencraft/dev/grove/-/commit/382e8a0129c6f640484069ad774e10db5e4b17f1))

**Features**

- Copy theme files into MFE build dir ([889cd98](https://gitlab.com/opencraft/dev/grove/-/commit/889cd9874b504d873115ab2337c401e951e561ab))
- Separate archive and delete pipelines ([7178dbd](https://gitlab.com/opencraft/dev/grove/-/commit/7178dbd98d765e691bc70380cbc1b835d94b8383))
- Add maintenance pages to Grove ([092b909](https://gitlab.com/opencraft/dev/grove/-/commit/092b9095e305cbcea2dd01cfc1c7904d6f68fdb4))
- Add minikube provider ([123a8fd](https://gitlab.com/opencraft/dev/grove/-/commit/123a8fdaa7396e239db5a59391584f844027f581))

**Miscellaneous Tasks**

- Update tutor-contrib-grove ([164b1b1](https://gitlab.com/opencraft/dev/grove/-/commit/164b1b1478b5ce951dc560fa6a16022eafab5c66))
- Bump tools-container version to 0.1.28 ([0e79198](https://gitlab.com/opencraft/dev/grove/-/commit/0e79198fb29627bc77e907fe8c908274ebef6ee7))
- Remove sql_require_primary_key override. no longer needed ([a57d85e](https://gitlab.com/opencraft/dev/grove/-/commit/a57d85e9361dde6371765147fc789047f2d2af82))
- Bump tools-container version ([b8b1b9b](https://gitlab.com/opencraft/dev/grove/-/commit/b8b1b9b1c00fa0c4548b311c41f6c84786824fca))
- Update container versions ([10c02e4](https://gitlab.com/opencraft/dev/grove/-/commit/10c02e4311040dac1d7b8949e512449a3bc2751b))
- Update tutor dependenices ([fccaec1](https://gitlab.com/opencraft/dev/grove/-/commit/fccaec1c895cbb9ef582597f1a32ff923abc6c14))

**Refactor**

- Use k8s-override patch ([251d5aa](https://gitlab.com/opencraft/dev/grove/-/commit/251d5aa38dba27b32d9be8d438df7acb54a69770))

## [0.2.0] - 2022-06-30

**Bug Fixes**

- Allow digital ocean spaces to be destroyed ([c7b23b7](https://gitlab.com/opencraft/dev/grove/-/commit/c7b23b70a619a3f698720bda8c4b31728b13dbbb))
- Gitlab aws runner aws version was not compatible with 4.10.0 ([9d7d14a](https://gitlab.com/opencraft/dev/grove/-/commit/9d7d14ac9f2de2a130b52fb48a3f8d2e436fafd5))

**Documentation**

- Add instance lifecycles documentation ([15c97a4](https://gitlab.com/opencraft/dev/grove/-/commit/15c97a42886399570f7b29ce3a5f3fa0f7bcbf34))
- Update contributing guide ([71b894b](https://gitlab.com/opencraft/dev/grove/-/commit/71b894b1244eb5ba4d4eed4abdbbd3439dc345f2))

**Features**

- Allow LMS and CMS env setting customization ([87ef1b5](https://gitlab.com/opencraft/dev/grove/-/commit/87ef1b588db2af3236070fff59873680441b3b9f))
- Implement Spaces storage for edx-platform in DO ([d1c2c9e](https://gitlab.com/opencraft/dev/grove/-/commit/d1c2c9ef5244b8814d5b128dce150550d0b9909b))
- Add HPA for k8s resources ([45250eb](https://gitlab.com/opencraft/dev/grove/-/commit/45250eb91ead17de4d560ec5d498952e2e5dc752))
- Seetup support for static files and static pages ([a493554](https://gitlab.com/opencraft/dev/grove/-/commit/a4935543689e0f77ca2cc5991607515425aa8268))
- Attach pod logs to notifier zip ([173cbc9](https://gitlab.com/opencraft/dev/grove/-/commit/173cbc93804aa4276088003efaf0f6440308a74d))
- Upgrade to tutor 14 ([d84839c](https://gitlab.com/opencraft/dev/grove/-/commit/d84839c79a19a4265e7cdbf347843fca1ed6e5a0))
- Add HPA configuration ([03a81b7](https://gitlab.com/opencraft/dev/grove/-/commit/03a81b714998e3e8111a9346e76a7a00ac257988))

**Miscellaneous Tasks**

- Prometheus now monitors all namespaces ([1bb065c](https://gitlab.com/opencraft/dev/grove/-/commit/1bb065cd00784aaac1c8817699c82633da1cd8a3))

## [0.1.0] - 2022-05-30

**Bug Fixes**

- Refactor s3 sync ([ca59f68](https://gitlab.com/opencraft/dev/grove/-/commit/ca59f68bcd121001d35c3de04149c3e360252b17))
- CI YAML indent issue ([9c87278](https://gitlab.com/opencraft/dev/grove/-/commit/9c87278dd228cebf40863c85e269e223a9ee31b9))
- Error during terraform init ([d10cb7f](https://gitlab.com/opencraft/dev/grove/-/commit/d10cb7f20f9012401a778218da69feaaa977514e))
- Tutor sync failing issue due to hyphen in name ([b77765e](https://gitlab.com/opencraft/dev/grove/-/commit/b77765e91a7580c42d845742cd8a71456e25feb0))
- Use the correct module paths for k8s ([2d9f55a](https://gitlab.com/opencraft/dev/grove/-/commit/2d9f55afc51655b9bc8e3a02079e3c251b2c4890))
- Remove yaml reserved characters from MongoDB password ([3e1d399](https://gitlab.com/opencraft/dev/grove/-/commit/3e1d3995a1839876bb00b9753b4ab612e050d444))
- Alert manager config parsing and added monitoring docs ([99bdae0](https://gitlab.com/opencraft/dev/grove/-/commit/99bdae0ab566b4dbd629e2f1b8c28d85e972bff2))
- Ensure env variable parsed correctly ([4237556](https://gitlab.com/opencraft/dev/grove/-/commit/4237556e9c6a953c686c0403a958d56aa3a5ba25))
- Update the k8s resources to include metadata ([eb8360c](https://gitlab.com/opencraft/dev/grove/-/commit/eb8360c2ba0b1e7d50c909df57d173b6bd965c4d))
- Generate users for db resources ([ea47764](https://gitlab.com/opencraft/dev/grove/-/commit/ea47764fdce584658536a2f9eb816365963a63c2))
- Support MySQL 8 on Tutor ([b1f495a](https://gitlab.com/opencraft/dev/grove/-/commit/b1f495a3d30e90067112e1e7efffb5fb6e2c50d5))
- Resolve several issues within Grove ([da028b5](https://gitlab.com/opencraft/dev/grove/-/commit/da028b520ef696e364ac21fdcb83aa1197dbc89d))
- Mongodb network container being created before the cluster ([734b236](https://gitlab.com/opencraft/dev/grove/-/commit/734b236b37574892f3f33caf9e38f2db23df0ced))
- Use kubectl context ([95a759e](https://gitlab.com/opencraft/dev/grove/-/commit/95a759eceaf66537c84353194bf19467c77bba67))
- Create instance on directory missing ([476e469](https://gitlab.com/opencraft/dev/grove/-/commit/476e469088d98fe7e1bec764dbe7710dadd8a35c))
- Allow empty commits ([0c9886c](https://gitlab.com/opencraft/dev/grove/-/commit/0c9886c88a0fc8132e70aeb28bd37d7c89c4f4e7))

**Documentation**

- Batch redeployment discovery ([c36cbbd](https://gitlab.com/opencraft/dev/grove/-/commit/c36cbbdf01302fb7e4146977d13c63574f60d41d))
- Ensure the documentation refers to the correct directory ([b18785f](https://gitlab.com/opencraft/dev/grove/-/commit/b18785f2abfd5c5ba17da0bbe46d44963b823488))
- Fix getting started typo ([0cf263d](https://gitlab.com/opencraft/dev/grove/-/commit/0cf263d5abfbf4d956a775e1204cd445c98ef94f))
- Added documentation on configuring new relic ([651e7f3](https://gitlab.com/opencraft/dev/grove/-/commit/651e7f3bbb12f455deca3a457e7268f1ed6b298a))
- Documented MongoDB Atlas setup for AWS ([ef63b40](https://gitlab.com/opencraft/dev/grove/-/commit/ef63b405497bca7d89cd0f978f1e5e9099633eea))
- Periodic builds discovery ([fe02761](https://gitlab.com/opencraft/dev/grove/-/commit/fe027613f1d48cb1eee996c0aa7c29c5edc742d5))
- Extend, reword and reorganize documentation ([6176978](https://gitlab.com/opencraft/dev/grove/-/commit/617697841fe09125a14a6075994a9572a5e92f8c))
- Add scaling deployments discovery ([431639f](https://gitlab.com/opencraft/dev/grove/-/commit/431639f16425f368a7f99c09ed46d77b01e44e07))
- Add documentation for commit-based pipelines ([5307ae3](https://gitlab.com/opencraft/dev/grove/-/commit/5307ae3bfe7f0219081af7415a16fb9a0e73f7ac))
- Add connecting to instances ([f20ab30](https://gitlab.com/opencraft/dev/grove/-/commit/f20ab303c2721817b9998e621843a2bed46bd5cc))
- Add contributing docs ([b6cf94b](https://gitlab.com/opencraft/dev/grove/-/commit/b6cf94bb6be54a1b3dbb11681faa35e3ce85a639))
- Add releases discovery ([9c75a47](https://gitlab.com/opencraft/dev/grove/-/commit/9c75a475d63cf1f46894ee4a65ac1aaf412707a9))
- Added documentation on the monitoring ingress ([c3e072a](https://gitlab.com/opencraft/dev/grove/-/commit/c3e072aafab5b8528b685e42109d273742ef4d9b))
- Add maintenance pages discovery ([3ce271b](https://gitlab.com/opencraft/dev/grove/-/commit/3ce271b48b7807151acef583a0091b331e66ccfc))
- Minor documentation improvements. ([5ab7f4b](https://gitlab.com/opencraft/dev/grove/-/commit/5ab7f4baf0b904f129975b55883c6accb72d7807))

**Features**

- Build tools container image ([6bc0da6](https://gitlab.com/opencraft/dev/grove/-/commit/6bc0da656b330c1ea1b20f0db75a009144af1ca6))
- Register gitlab with digitalocean kubernetes ([0db6ec1](https://gitlab.com/opencraft/dev/grove/-/commit/0db6ec11e61ac88e42fd2199d71e90567d743c7e))
- Create CI job to generate deployment Merge Requests ([c7a56e9](https://gitlab.com/opencraft/dev/grove/-/commit/c7a56e955e6d99d3c92aebef59cd24f0f051fc2b))
- AWS backups ([dcab4f6](https://gitlab.com/opencraft/dev/grove/-/commit/dcab4f6cb3973680da027c06f3e5c4b3795b7d83))
- DigitalOcean backups ([f4d06ee](https://gitlab.com/opencraft/dev/grove/-/commit/f4d06ee96703e0167e0f66cff2535aa26f92c394))
- Implement DigitalOcean provider and refactor AWS one ([e56fae1](https://gitlab.com/opencraft/dev/grove/-/commit/e56fae195656051db42051466ddfd4a5c41774ef))
- Add prometheus and grafana dashboards ([dd97269](https://gitlab.com/opencraft/dev/grove/-/commit/dd972696c77d98d5eede28bdbdf171d9646b5703))
- Adds new relic monitoring for grove ([550b07f](https://gitlab.com/opencraft/dev/grove/-/commit/550b07f1002a002885618ded62b2df41ecc3706b))
- OpenSearch security configuration [SE-5432] ([f9316af](https://gitlab.com/opencraft/dev/grove/-/commit/f9316aff978c2bb557cfafb600f0e8a965524343))
- Use gitlab cluster agent instead of certificates[se-5533] ([912ad99](https://gitlab.com/opencraft/dev/grove/-/commit/912ad997e2c4599d756e5fb284b5760f9fe79966))
- Ingress and ssl for monitoring ([d99ebbe](https://gitlab.com/opencraft/dev/grove/-/commit/d99ebbefdbb8a04bf959d711e672f726a122b9b7))
- Instance archive pipeline ([c579ea2](https://gitlab.com/opencraft/dev/grove/-/commit/c579ea29fa1b4a7e0691039557010bd9bcadde43))
- Grafana dashboards for monitoring instance usage ([41d431d](https://gitlab.com/opencraft/dev/grove/-/commit/41d431d409b201c8303842288e55ad7d5de3a9c0))
- Implement periodic email notifications ([03261d6](https://gitlab.com/opencraft/dev/grove/-/commit/03261d6b3c8488ac0e4b6caacdbf5c71ac38db01))
- Implement Spaces storage for edx-platform in DO ([375248e](https://gitlab.com/opencraft/dev/grove/-/commit/375248eb3db2b2ca7496c24f09ae37b82da25529))
- Implement Spaces storage for edx-platform in DO ([f991bef](https://gitlab.com/opencraft/dev/grove/-/commit/f991befc9dac6708d64db6410fb855866cd92ac1))
- Add HPA for k8s resources ([0efdc55](https://gitlab.com/opencraft/dev/grove/-/commit/0efdc55f08cdcd340c022c6917ca128d42700fa6))
- Seetup support for static files and static pages ([e0b7ad8](https://gitlab.com/opencraft/dev/grove/-/commit/e0b7ad80a1d583b0fdeb74249308f56ef3e2173d))
- Create a VPC ([49ad00b](https://gitlab.com/opencraft/dev/grove/-/commit/49ad00b18f66952ca2be92e583905dde28952282))

**Fix**

- Terraform state wasn't saved to GitLab ([c3745c8](https://gitlab.com/opencraft/dev/grove/-/commit/c3745c8c33ab1c7709a941cb983ed3edfac2339c))
- Add in kubernetes provider configuration as required ([244d163](https://gitlab.com/opencraft/dev/grove/-/commit/244d16336e2b0866607f4dd7fc5b47edcc23f8e3))

**Miscellaneous Tasks**

- Explicitly pin click ([0c46c67](https://gitlab.com/opencraft/dev/grove/-/commit/0c46c67e63b3087a31dfc2f5c8c409678dcb17b6))
- Upgrade DO provider ([da5d020](https://gitlab.com/opencraft/dev/grove/-/commit/da5d0202706c8b01228bddd1efbf66125c084cf8))
- Bump tools-container version ([0ee912d](https://gitlab.com/opencraft/dev/grove/-/commit/0ee912dbe89ecd899d52022da22417bcae165142))
- Bump tools-container version ([c55fe66](https://gitlab.com/opencraft/dev/grove/-/commit/c55fe666b8d614167205594b0f26f48340f2891c))
- Bump default grove version ([249d4ad](https://gitlab.com/opencraft/dev/grove/-/commit/249d4ad1fd34c83b8486090a0c30cfdc25099d25))
- Updated aws s3 resources to new versions ([c12dd7d](https://gitlab.com/opencraft/dev/grove/-/commit/c12dd7d056f625ce4e966f6c0a9a09b7e7c0a6b5))
- Amazon EKS version upgraded to v18. ([343913a](https://gitlab.com/opencraft/dev/grove/-/commit/343913aba38a40df3e14d67381e49108d23a81e1))
- Prometheus now monitors all namespaces ([10f58ca](https://gitlab.com/opencraft/dev/grove/-/commit/10f58ca50830c940a4f26cac97e9aef3e7dbd299))
- Sync 'env' using object storage ([78fa0e8](https://gitlab.com/opencraft/dev/grove/-/commit/78fa0e852827108d238d22ce6bd1ef72086af157))

**Refactor**

- Use 'control' directory for all local commands ([aa887fe](https://gitlab.com/opencraft/dev/grove/-/commit/aa887febadc9daccc9852bcaee08d7fd0fe5b1ed))
- Remove the director instance ([0e8ff24](https://gitlab.com/opencraft/dev/grove/-/commit/0e8ff2460fcbe0558885097d0865f71eae846fa2))
- Remove redundancy from mysql outputs ([9e1cfc8](https://gitlab.com/opencraft/dev/grove/-/commit/9e1cfc86fd4ac3f78c6308b7c18c2aae6d5712db))
- Temporarily use a map for env buckets on DO ([e5dd66e](https://gitlab.com/opencraft/dev/grove/-/commit/e5dd66e139dbdadbc92497fb0871fccd4e5aff33))
- Replace managed redis with a helm chart ([769f263](https://gitlab.com/opencraft/dev/grove/-/commit/769f2630f3f495de6f2719fd3d3b201e2bb80c7a))
- Use-single-bucket ([acdd55c](https://gitlab.com/opencraft/dev/grove/-/commit/acdd55c69662272c58493df9bdefcebbe629eb01))
- Removed dead code and cleaned up files ([45e4d76](https://gitlab.com/opencraft/dev/grove/-/commit/45e4d76997ca5291a24847e25dbc790a30a26e07))
- Users can no longer set the monitoring namespace ([e2c0e62](https://gitlab.com/opencraft/dev/grove/-/commit/e2c0e62340b3cb7a12cfb1706afb9e99f4a1d08e))

**Build**

- Fixes multiple issues with the ci ([a9e7766](https://gitlab.com/opencraft/dev/grove/-/commit/a9e776605616336c1fff40752a515cbb4abc6a79))
- Tools-container version updated to 0.1.12 ([3cbed2f](https://gitlab.com/opencraft/dev/grove/-/commit/3cbed2fdeab1b0f896e27bdd38fbb78faddf8ef3))
- Updated tools-container version ([ff1f9c2](https://gitlab.com/opencraft/dev/grove/-/commit/ff1f9c2cb136f1b70aa4af8333d0af311205c1bf))
- Updated terraform providers and tools-container to latest ([2ea120b](https://gitlab.com/opencraft/dev/grove/-/commit/2ea120b6404dd64813bdfbdaebe74305221dcc33))
- Docs dependencies are on latest versions ([1ac8dd0](https://gitlab.com/opencraft/dev/grove/-/commit/1ac8dd0410f197eb7acc11b20b1e8fcdea8af865))
- Updated nginx ingress, cert manager, droplets, mongo to latest ([5b78841](https://gitlab.com/opencraft/dev/grove/-/commit/5b78841ec5d89dc6c6404fb56a194d3bccb3735c))
- Larger instances required. added security rules for mysql ([81da288](https://gitlab.com/opencraft/dev/grove/-/commit/81da288c0192d95c27db0e7232a4a1a803e062ab))

**Ci**

- Fix cobertura key path ([31e17f6](https://gitlab.com/opencraft/dev/grove/-/commit/31e17f64f4890a2324ae280fb517816cfcdef0e4))
