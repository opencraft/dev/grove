# k8s-monitoring

This module contains resources related to monitoring.

- [Fluent-bit](https://github.com/fluent/helm-charts/tree/main/charts/fluent-bit)
- [OpenSearch](https://github.com/opensearch-project/helm-charts/charts/opensearch)
- [OpenSearch Dashboards](https://github.com/opensearch-project/helm-charts/charts/opensearch-dashboards)

# Variables:

- `cluster_domain`: Base domain for all monitoring services.
- `enable_monitoring_ingress`: Boolean that determines whether to enable ingress for monitoring services. Defaults to `false`.
- `fluent_bit_aws_config`: An object/hash consisting of the keys:
  - `aws_access_key_id`: Your AWS/DO account's access key.
  - `aws_secret_access_key`: Your AWS/DO account's secret key.
  - `s3_bucket_name`: Your S3 bucket name.
  - `s3_region`: The region the S3 bucket is contained in.
  - `s3_endpoint`: If using a provider other than AWS, this needs to be provided.
  - `cloudwatch_group_name`: The Cloudwatch group name to push the logs to.
  - `cloudwatch_region`: The region your Cloudwatch group is in.
- `opensearch_resource_quotas`: The [Kubernetes resource configuration](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) for the OpenSearch pods.
- `opensearch_num_replicas`: The number of OpenSearch replicas to create. Defaults to `1`.
- `opensearch_index_retention_days`: The number of days to keep logs for. Defaults to `180`.
- `opensearch_persistence_size`: The size of the OpenSearch PV. Defaults to `8Gi`. Cannot be modified once the resources are created.

# Components

## Fluent-bit

Logs are collected for all containers running on the cluster and forwarded to the OpenSearch dashboards
by default.

Forwarding the logs to an S3 backed storage or AWS Cloudwatch is supported, by setting the appropriate configuration
in the `fluent_bit_aws_config`, `fluent_bit_aws_tracking_log_config` and `fluent_bit_aws_kube_log_config` variables.

For more information, visit https://grove.opencraft.com/monitoring/tracking-logs/.

### S3

Logs in s3 will be placed in the `fluent-bit-logs` directory in your S3 bucket. Tracking logs will be
within the `fluent-bit-tracking-logs/` directory separated by date.

All other logs will be of the format `/logs/tracking/%Y/%m/%d/tracking.log-%Y%m%d-%H%M%S.gz`.

Be aware that the key format should be a valid fluent-bit format. For more information, visit https://docs.fluentbit.io/manual/pipeline/outputs/s3.

### Cloudwatch

For Cloudwatch two streams will be created,

- `fluent-bit-logs`: All logs including the tracking logs will be available here.
- `fluent-bit-tracking-logs`: Only tracking logs will be available here.

## OpenSearch Dashboard

Cluster logs are forwarded using Fluent-bit to OpenSearch. They can be accessed via the OpenSearch dashboard.

Accessing the dashboard is possible by running the following command within the `control` directory.

```bash
./kubectl --namespace monitoring port-forward --address 0.0.0.0 deployments/opensearch-dashboard-opensearch-dashboards 8001:5601
```

The username is `admin` and the password can be retrieved with:

```bash
./tf output -raw opensearch_dashboard_admin_password
```

After running the command above, the default OpenSearch dashboard will be viewable
in your browser at [http://localhost:8001](http://localhost:8001).

On the first run, you will need to create
an **Index Pattern** for `fluent-bit`. Once done, you will be able to view
the logs in [your discover page](http://localhost:8001/app/discover).

# Ingress

## Domains
The `TF_VAR_cluster_domain` variable contains the base domain used for the monitoring resources.

Ingresses will be created for each of the above as `<SERVICE NAME>.${cluster_domain}`.

- `opensearch-dashboards.${cluster_domain}`

## Enable

Ingress for monitoring services can be enabled by setting the variable `enable_monitoring_ingress` to true.

**Please note that you need to set up DNS before applying the change otherwise the apply will fail.**

## Access

To prohibit unauthorized to these, basic HTTP authentication is set up for extra protection.

The username is `admin` and the password can be obtained with `./tf output -raw monitoring_ingress_password`.

# Output

This module doesn't provide any output.
