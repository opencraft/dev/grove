####################################################################################################
## Monitoring: Resources for monitoring our Kubernetes cluster
####################################################################################################

# This provider is required for stateful updates of passwords.
# OpenSearch Dashboard needs a bcrypt hash to configure it's password.
# The built-in terraform `random_password` module + the bcrypt function
# works, but it changes every time terraform is run requiring in a
# rebuild of resources.
# https://github.com/hashicorp/terraform-provider-random/issues/102

terraform {
  required_providers {
    htpasswd = {
      source  = "loafoe/htpasswd"
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
    }
  }
}

locals {
  namespace_monitoring         = "monitoring"
  opensearch_resource_limits   = try(var.opensearch_resource_quotas["limits"], null)
  opensearch_resource_requests = try(var.opensearch_resource_quotas["requests"], null)
  opensearch_endpoint          = "opensearch.${var.cluster_domain}"
  cleanup_policy               = "cleanup-policy"
  opensearch_version           = "2.17.2" # https://github.com/opensearch-project/helm-charts/releases
  opensearch_dashboard_version = "2.15.1" # https://github.com/opensearch-project/helm-charts/releases
  fluentbit_version            = "0.43.0" # https://github.com/fluent/helm-charts/releases
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = local.namespace_monitoring
  }
}

resource "random_password" "opensearch_admin_password" {
  length  = 32
  special = false
  keepers = {
    iteration = "1"
  }
}

resource "htpasswd_password" "hash" {
  password = random_password.opensearch_admin_password.result
  salt     = substr(sha512(random_password.opensearch_admin_password.result), 0, 8)
}

# This is not used at the moment, but is required for the OpenSearch securityConfig
# https://github.com/opensearch-project/helm-charts/issues/125
resource "kubernetes_secret" "opensearch_secret" {
  depends_on = [kubernetes_namespace.monitoring]
  metadata {
    name        = "internal-users-config-secret"
    namespace   = kubernetes_namespace.monitoring.metadata[0].name
    annotations = {}
    labels      = {}
  }

  data = {
    "internal_users.yml" = templatefile("${path.module}/internal_users.yml", {
      hashed_password = htpasswd_password.hash.bcrypt
    })
  }

  type = "Opaque"
}

resource "helm_release" "opensearch_cluster" {
  name       = "opensearch"
  repository = "https://opensearch-project.github.io/helm-charts/"
  chart      = "opensearch"
  version    = local.opensearch_version
  namespace  = kubernetes_namespace.monitoring.metadata[0].name

  values = [
    templatefile("${path.module}/opensearch_values.yml", {
      resource_limits   = local.opensearch_resource_limits,
      resource_requests = local.opensearch_resource_requests,
      num_replicas      = var.opensearch_num_replicas,
      persistence_size  = var.opensearch_persistence_size
    })
  ]
}

resource "helm_release" "opensearch_dashboard" {
  depends_on = [helm_release.opensearch_cluster]
  name       = "opensearch-dashboard"
  repository = "https://opensearch-project.github.io/helm-charts/"
  chart      = "opensearch-dashboards"
  version    = local.opensearch_dashboard_version
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  values = [
    "${templatefile("${path.module}/dashboard_values.yml", { password = "${random_password.opensearch_admin_password.result}" })}",
  ]
}

resource "kubernetes_secret" "fluent_bit_aws_keys" {
  depends_on = [kubernetes_namespace.monitoring]

  metadata {
    name      = "fluent-bit-aws-keys"
    namespace = kubernetes_namespace.monitoring.metadata[0].name
  }

  data = {
    "AWS_ACCESS_KEY_ID" : var.fluent_bit_aws_config.aws_access_key_id
    "AWS_SECRET_ACCESS_KEY" : var.fluent_bit_aws_config.aws_secret_access_key
  }
  type = "Opaque"
}

resource "helm_release" "fluent_bit" {
  depends_on = [helm_release.opensearch_cluster]
  name       = "fluent-bit"
  repository = "https://fluent.github.io/helm-charts"
  chart      = "fluent-bit"
  version    = local.fluentbit_version
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  timeout    = 120

  values = [
    templatefile(
      "${path.module}/fluent_bit_values.yml", {
        password             = random_password.opensearch_admin_password.result,
        kube_logs_config     = var.fluent_bit_aws_kube_log_config,
        tracking_logs_config = var.fluent_bit_aws_tracking_log_config
    })
  ]
}

resource "null_resource" "create_and_attach_cleanup_policy" {
  depends_on = [helm_release.opensearch_cluster]
  count      = var.enable_monitoring_ingress ? 1 : 0

  provisioner "local-exec" {
    command = "python3 ${path.module}/create-cleanup-policy.py"
    environment = {
      OPENSEARCH_HOST = local.opensearch_endpoint
      PASSWORD        = random_password.opensearch_admin_password.result
      POLICY_ID       = local.cleanup_policy,
      POLICY = templatefile("${path.module}/opensearch_cleanup_policy.json", {
        retention_days = var.opensearch_index_retention_days
      })
    }
  }
}
