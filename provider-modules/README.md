# Provider-modules

This directory contains all provider independent modules.

## Available modules

* [`k8s-dashboard`](./k8s-dashboard): Installs Kubernetes Dashboard using Helm
* [`k8s-gitlab-connector`](./k8s-gitlab-connector): Creates a connection between a GitLab project and a Kubernetes cluster
* [`k8s-gitlab-container-registry`](./k8s-gitlab-container-registry): Creates a namespace, secret set and service account to connect to GitLab Container Registry
* [`k8s-nginx-ingress`](./k8s-ingress): Installs [NGINX Ingress Controller](https://kubernetes.github.io/ingress-nginx/) and [cert-manager](https://cert-manager.io/docs/) using Helm

For further information, please read the module's documentation.
