variable "tutor_instances" {
  type        = list(string)
  description = "List of instances to collect configuration for."
}

variable "tutor_instances_dir" {
  type        = string
  description = "Directory where instances are defined."
}
