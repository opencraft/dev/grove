locals {
  configs = {
    for instance in var.tutor_instances :
    instance => yamldecode(file("${var.tutor_instances_dir}/${instance}/config.yml"))
  }

  instance_domains = tomap({
    for instance in var.tutor_instances :
    instance => compact(flatten([
      local.configs[instance]["LMS_HOST"],
      try(local.configs[instance]["CMS_HOST"], "studio.${local.configs[instance]["LMS_HOST"]}"),
      try(local.configs[instance]["MFE_HOST"], "app.${local.configs[instance]["LMS_HOST"]}"),
      try(local.configs[instance]["DISCOVERY_HOST"], "discovery.${local.configs[instance]["LMS_HOST"]}"),
      try(local.configs[instance]["ECOMMERCE_HOST"], "ecommerce.${local.configs[instance]["LMS_HOST"]}"),
      try(local.configs[instance]["PREVIEW_LMS_HOST"], "preview.${local.configs[instance]["LMS_HOST"]}"),
      [
        for record in try(local.configs[instance]["GROVE_ADDITIONAL_DOMAINS"], []) :
        try(record["domain"], null)
      ],
    ]))
  })
}

output "configs" {
  value = local.configs
}

output "instance_domains" {
  value = local.instance_domains
}

output "allowed_cors_origins" {
  value = tomap({
    for instance, domains in local.instance_domains :
    instance => formatlist("https://%s", domains)
  })
}
