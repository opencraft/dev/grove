# instance

This module does nothing, but collects the `config.yml` content of instances.

# Variables

- `tutor_instances`: List of instances to collect configuration for.
- `tutor_instances_dir`: Directory where instances are defined.

# Output

- `configs`: Map of instance names to their parsed YAML configuration.
