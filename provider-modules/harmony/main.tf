terraform {
  required_providers {
    htpasswd = {
      source = "loafoe/htpasswd"
    }

    kubectl = {
      source = "gavinbunney/kubectl"
    }

    helm = {
      source = "hashicorp/helm"
    }
  }
}

locals {
  # Note: When updating the Harmony helm chart, ensure that the CRDs are updated
  # as well. Otherwise, the components installed by the Harmony chart will not
  # work as expected. The CRDs are stored in the assets directory.
  #
  # Prometheus CRDs: https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack/charts/crds/crds
  # Cert Manager CRDs: https://github.com/cert-manager/cert-manager/tree/master/deploy/crds
  harmony_chart_version = "0.9.4"
  harmony_namespace     = "harmony"

  harmony_crds = concat(
    split("---", templatefile("${path.module}/assets/certmanager-crds.1.13.3.yaml", {})),
    [for crd in concat(
      # Alertmanager
      var.alertmanager_enabled ? [
        "${path.module}/assets/crd-alertmanagerconfigs.yaml",
      ] : [],

      # Prometheus
      var.prometheus_enabled ? [
        "${path.module}/assets/crd-alertmanagers.yaml",
        "${path.module}/assets/crd-podmonitors.yaml",
        "${path.module}/assets/crd-probes.yaml",
        "${path.module}/assets/crd-prometheusagents.yaml",
        "${path.module}/assets/crd-prometheuses.yaml",
        "${path.module}/assets/crd-prometheusrules.yaml",
        "${path.module}/assets/crd-scrapeconfigs.yaml",
        "${path.module}/assets/crd-servicemonitors.yaml",
        "${path.module}/assets/crd-thanosrulers.yaml",
      ] : []
    ) : file(crd)]
  )

  harmony_alertmanager_config = var.alertmanager_enabled ? merge(
    yamldecode(file("${path.module}/assets/alertmanager-default.yaml")),
    yamldecode(var.alertmanager_config)
  ) : null

  prometheus_critical_alerts = templatefile("${path.module}/assets/prometheus-critical-alerts.yaml", {
    namespace = local.harmony_namespace
  })
  prometheus_combined_alerts = var.alertmanager_enabled ? "${local.prometheus_critical_alerts}\n${var.prometheus_additional_alerts}" : ""

  # https://github.com/openfaas/faas-netes/blob/master/chart/cron-connector/Chart.yaml
  openfaas_connector_chart_version = "0.6.11"

  elasticsearch_config = {
    heap_size         = var.elasticsearch_config.heap_size == null ? "2g" : var.elasticsearch_config.heap_size
    search_queue_size = var.elasticsearch_config.search_queue_size == null ? 5000 : var.elasticsearch_config.search_queue_size
    cpu_limit         = var.elasticsearch_config.cpu_limit == null ? "1000m" : var.elasticsearch_config.cpu_limit
    memory_limit      = var.elasticsearch_config.memory_limit == null ? "4Gi" : var.elasticsearch_config.memory_limit
  }
}

####################################################################################################
## Harmony cluster dependencies
####################################################################################################

resource "kubernetes_namespace" "harmony_namespace" {
  metadata {
    name = local.harmony_namespace
  }
}

resource "kubernetes_config_map" "custom_error_pages" {
  metadata {
    name      = "custom-error-pages"
    namespace = kubernetes_namespace.harmony_namespace.metadata[0].name
  }

  data = {
    "404" = file("${path.module}/assets/ingress-404.html")
  }
}

# NOTE: Because of a bug in the helm_resource provider, we need to install CRDs
# manually using kubectl_manifest resources: https://github.com/hashicorp/terraform-provider-helm/issues/976

resource "kubectl_manifest" "harmony_chart_dependency_crds" {
  count              = length(local.harmony_crds)
  depends_on         = [var.cluster_id]
  override_namespace = kubernetes_namespace.harmony_namespace.metadata[0].name
  yaml_body          = local.harmony_crds[count.index]
  server_side_apply  = true
  force_new          = true
  force_conflicts    = true
}

####################################################################################################
## Grafana admin and dashboards setup
####################################################################################################

resource "random_password" "grafana_admin" {
  count = var.grafana_enabled ? 1 : 0

  length           = 32
  special          = false
  override_special = ",.-_!"
}

resource "kubernetes_config_map" "grafana_extra_dashboards" {
  count = var.grafana_enabled ? 1 : 0

  binary_data = {}

  metadata {
    name        = "grafana-extra-dashboards"
    namespace   = kubernetes_namespace.harmony_namespace.metadata[0].name
    annotations = {}
    labels      = {}
  }

  data = {
    "default-dashboard.json" : file("${path.module}/assets/kubernetes-views-global_rev8.json")
  }
}

####################################################################################################
## OpenFAAS and cron connector setup
####################################################################################################

resource "random_password" "openfaas_admin_password" {
  count = var.openfaas_enabled ? 1 : 0

  length           = 32
  special          = false
  override_special = ",.-_!"
}

resource "kubernetes_secret" "openfaas_basic_auth" {
  count = var.openfaas_enabled ? 1 : 0

  metadata {
    name      = "basic-auth"
    namespace = kubernetes_namespace.harmony_namespace.metadata[0].name
  }

  # Username and password can be anything, since we are doing basic auth
  # through the internal gatewayURL, so basic auth is not protected.
  data = {
    "basic-auth-user"     = "admin",
    "basic-auth-password" = random_password.openfaas_admin_password[0].result,
  }
}

resource "helm_release" "openfaas_cron_connector" {
  count = var.openfaas_enabled ? 1 : 0

  name       = "openfaas-cron-connector"
  repository = "https://openfaas.github.io/faas-netes"
  chart      = "cron-connector"
  version    = local.openfaas_connector_chart_version
  namespace  = kubernetes_namespace.harmony_namespace.metadata[0].name

  set {
    name  = "gatewayURL"
    value = "http://gateway.${kubernetes_namespace.harmony_namespace.metadata[0].name}.svc.cluster.local:8080"
  }

  depends_on = [
    kubernetes_secret.openfaas_basic_auth[0],
    helm_release.harmony
  ]
}

####################################################################################################
## Elasticsearch certificates
####################################################################################################

data "kubernetes_secret" "elasticsearch_certificates" {
  count      = var.elasticsearch_enabled ? 1 : 0
  depends_on = [helm_release.harmony]

  metadata {
    name      = "elasticsearch-certificates"
    namespace = kubernetes_namespace.harmony_namespace.metadata[0].name
  }
}

####################################################################################################
## Harmony chart configuration
####################################################################################################

resource "helm_release" "harmony" {
  timeout = 1800
  depends_on = [
    kubectl_manifest.harmony_chart_dependency_crds,
    kubernetes_config_map.custom_error_pages,
  ]

  repository       = "https://openedx.github.io/openedx-k8s-harmony"
  name             = "openedx-harmony"
  chart            = "harmony-chart"
  namespace        = kubernetes_namespace.harmony_namespace.metadata[0].name
  version          = local.harmony_chart_version
  create_namespace = true

  values = [
    templatefile("${path.module}/values.yml", {
      # Global configuration
      cluster_domain : var.cluster_domain,
      cluster_provider : var.cluster_provider,
      k8s_dashboard_enabled : var.k8s_dashboard_enabled,
      k8s_dashboard_host : coalesce(var.k8s_dashboard_host, "k8s-dashboard.${var.cluster_domain}"),

      # Ingress configuration
      ingress_resource_limits : lookup(var.ingress_resource_quota, "limits", null),
      ingress_resource_requests : lookup(var.ingress_resource_quota, "requests", null),

      # Cert manager configuration
      cert_manager_email : var.lets_encrypt_notification_inbox,

      # Monitoring configuration
      prometheus_enabled : var.prometheus_enabled,

      grafana_enabled : var.grafana_enabled,
      grafana_host : coalesce(var.grafana_host, "grafana.${var.cluster_domain}"),
      grafana_admin_password : try(random_password.grafana_admin[0].result, ""),

      alertmanager_enabled : var.alertmanager_enabled,
      alertmanager_config : local.harmony_alertmanager_config,

      # Velero configuration
      velero_enabled : var.velero_enabled,
      velero_backup_bucket : var.velero_backup_bucket,
      velero_backup_region : var.velero_backup_region,
      velero_access_key_id : var.velero_backup_access_key_id,
      velero_secret_access_key : var.velero_backup_secret_access_key,
      velero_plugin_aws_version : var.velero_plugin_aws_version,
      velero_plugin_digitalocean_version : var.velero_plugin_digitalocean_version,
      velero_plugin_digitalocean_token : var.velero_plugin_digitalocean_token,
      velero_volume_snapshot_provider : var.velero_volume_snapshot_provider,

      # OpenFAAS configuration
      openfaas_enabled : var.openfaas_enabled,
      openfaas_host : coalesce(var.openfaas_host, "openfaas.${var.cluster_domain}"),

      # Elasticsearch configuration
      elasticsearch_enabled : var.elasticsearch_enabled,
      elasticsearch_heap_size : local.elasticsearch_config.heap_size,
      elasticsearch_queue_size : local.elasticsearch_config.search_queue_size,
      elasticsearch_cpu_limit : local.elasticsearch_config.cpu_limit,
      elasticsearch_memory_limit : local.elasticsearch_config.memory_limit,
    })
  ]
}

resource "kubectl_manifest" "prometheus_rules" {
  count      = var.prometheus_enabled && var.alertmanager_enabled ? 1 : 0
  depends_on = [helm_release.harmony]
  yaml_body  = local.prometheus_combined_alerts
}
