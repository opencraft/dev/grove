clusterDomain: ${ cluster_domain }
notificationEmail: ${ cert_manager_email }

ingress-nginx:
  enabled: true
  rbac:
    create: true
  controller:
    admissionWebhooks:
      enabled: true
    %{ if ingress_resource_limits != null || ingress_resource_requests != null }
    resources:
      %{ if ingress_resource_limits != null }
      limits:
        %{ if try(ingress_resource_limits["cpu"], null) != null }cpu: ${ingress_resource_limits["cpu"]}%{ endif }
        %{ if try(ingress_resource_limits["memory"], null) != null }memory: ${ingress_resource_limits["memory"]}%{ endif }
      %{ endif ~}
      %{ if ingress_resource_requests != null }
      requests:
        %{ if try(ingress_resource_requests["cpu"], null) != null }cpu: ${ingress_resource_requests["cpu"]}%{ endif }
        %{ if try(ingress_resource_requests["memory"], null) != null ~}memory: ${ingress_resource_requests["memory"]}%{ endif ~}
      %{ endif }
    %{ endif }
    custom-http-errors: "404"
    config:
      use-proxy-protocol: "true"
      use-forwarded-headers: "true"
      compute-full-forwarded-for: "true"
    service:
      annotations:
        %{~ if cluster_provider == "aws" ~}
        service.beta.kubernetes.io/aws-load-balancer-proxy-protocol: "*"
        %{~ endif ~}
        %{~ if cluster_provider == "digitalocean" ~}
        service.beta.kubernetes.io/do-loadbalancer-enable-proxy-protocol: "true"
        service.beta.kubernetes.io/do-loadbalancer-hostname: "${ cluster_domain }"
        %{~ endif ~}
  defaultBackend:
    enabled: true
    image:
      registry: k8s.gcr.io
      image: ingress-nginx/nginx-errors
      tag: "0.48.1"
    extraVolumes:
    - name: custom-error-pages
      configMap:
        name: custom-error-pages
        items:
        - key: "404"
          path: "404.html"
    extraVolumeMounts:
    - name: custom-error-pages
      mountPath: /www

cert-manager:
  enabled: true
  global:
    podSecurityPolicy:
      enabled: false
      useAppArmor: true
  prometheus:
    enabled: false

metricsserver:
  enabled: true
  serviceAccount:
    create: true
    name: "metrics-server"
  metrics:
    enabled: true

k8sdashboard:
  enabled: ${ k8s_dashboard_enabled }
  cert-manager:
    enabled: false
  metrics-server:
    enabled: false
  kong:
    enabled: false
  app:
    ingress:
      enabled: true
      ingressClassName: nginx
      annotations:
        cert-manager.io/cluster-issuer: "harmony-letsencrypt-global"
        nginx.ingress.kubernetes.io/ssl-redirect: "true"
      hosts:
        - ${ k8s_dashboard_host }
      tls:
        - secretName: k8s-dashboard-ingress-tls
          hosts:
            - ${ k8s_dashboard_host }

prometheusstack:
  enabled: ${ prometheus_enabled }

  prometheus:
    prometheusSpec:
      scrapeInterval: 30s
      evaluationInterval: 30s
      ruleSelector: {}
      ruleNamespaceSelector: {}
      ruleSelectorNilUsesHelmValues: false
      serviceMonitorSelector: {}
      serviceMonitorNamespaceSelector: {}
      serviceMonitorSelectorNilUsesHelmValues: false
      podMonitorSelector: {}
      podMonitorNamespaceSelector: {}
      podMonitorSelectorNilUsesHelmValues: false

  grafana:
    enabled: ${ grafana_enabled }

    ingress:
      enabled: true
      ingressClassName: nginx
      annotations:
        cert-manager.io/cluster-issuer: "harmony-letsencrypt-global"
        nginx.ingress.kubernetes.io/ssl-redirect: "true"
      hosts:
        - ${ grafana_host }
      tls:
        - secretName: promstack-ingress-tls
          hosts:
            - ${ grafana_host }

    defaultDashboardsEnabled: true
    defaultDashboardsTimezone: browser
    adminPassword: ${ grafana_admin_password }
    service:
      enabled: true
      type: ClusterIP
      port: 3000
      targetPort: 3000
      annotations: {}
      labels: {}
      portName: service

    extraConfigmapMounts:
      - name: grafana-extra-dashboards
        mountPath: /tmp/dashboards/extra/
        configMap: grafana-extra-dashboards
        readOnly: false

    dashboards:
      default:
        kubernetes-views:
          file: /tmp/dashboards/extra/default-dashboard.json

    grafana.ini:
      dashboards:
        default_home_dashboard_path: /tmp/dashboards/extra/default-dashboard.json

  %{~ if alertmanager_enabled ~}
  alertmanager:
    enabled: true
    config: ${ jsonencode(alertmanager_config) }
  %{~ endif ~}

velero:
  enabled: ${ velero_enabled }

  deployNodeAgent: false

  configuration:
    backupStorageLocation:
      - name: default
        provider: aws
        bucket: ${ velero_backup_bucket }
        default: true
        config:
          s3Url: %{ if velero_plugin_digitalocean_version != null }"https://${ velero_backup_region }.digitaloceanspaces.com"%{ else }"https://s3.${ velero_backup_region }.amazonaws.com"%{ endif }
          region: ${ velero_backup_region }

    volumeSnapshotLocation:
      - name: default
        provider: ${ velero_volume_snapshot_provider }
        config:
          region: ${ velero_backup_region }

  credentials:
    extraEnvVars:
        %{ if velero_plugin_digitalocean_token != null }DIGITALOCEAN_TOKEN: "${ velero_plugin_digitalocean_token }"%{ endif }
    secretContents:
      cloud: |
        [default]
        aws_access_key_id="${ velero_access_key_id }"
        aws_secret_access_key="${ velero_secret_access_key }"

  initContainers:
    - name: velero-plugin-for-aws
      image: velero/velero-plugin-for-aws:${ velero_plugin_aws_version }
      volumeMounts:
        - mountPath: /target
          name: plugins

    %{ if velero_plugin_digitalocean_version != null }
    - name: velero-plugin-for-digitalocean
      image: digitalocean/velero-plugin:${ velero_plugin_digitalocean_version }
      volumeMounts:
        - mountPath: /target
          name: plugins
    %{ endif }

  schedules:
    hourly-backup:
      disabled: false
      schedule: "30 */1 * * *"
      template:
        ttl: "24h"
    daily-backup:
      disabled: false
      schedule: "0 6 * * *"
      template:
        ttl: "168h"
    weekly-backup:
      disabled: false
      schedule: "59 23 * * 0"
      template:
        ttl: "720h"

openfaas:
  enabled: ${ openfaas_enabled }
  exposeServices: false
  generateBasicAuth: false
  ingress:
    enabled: true
    ingressClassName: nginx
    annotations:
      cert-manager.io/cluster-issuer: "harmony-letsencrypt-global"
      nginx.ingress.kubernetes.io/ssl-redirect: "true"
      nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    hosts:
      - host: ${ openfaas_host }
        http:
          paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: gateway
                port:
                  number: 8080
    tls:
      - secretName: openfaas-ingress-tls
        hosts:
          - ${ openfaas_host }

elasticsearch:
  enabled: ${ elasticsearch_enabled }

  annotations:
    app.kubernetes.io/name: elasticsearch

  esJavaOpts: "-Xmx${elasticsearch_heap_size} -Xms${elasticsearch_heap_size}"

  esConfig:
    "elasticsearch.yml": |
      cluster.name: harmony-search-cluster
      xpack.security.enabled: true
      xpack.security.http.ssl.enabled: true
      xpack.security.http.ssl.key: /usr/share/elasticsearch/config/certs/tls.key
      xpack.security.http.ssl.certificate: /usr/share/elasticsearch/config/certs/tls.crt
      xpack.security.transport.ssl.enabled: true
      xpack.security.transport.ssl.key: /usr/share/elasticsearch/config/certs/tls.key
      xpack.security.transport.ssl.certificate: /usr/share/elasticsearch/config/certs/tls.crt
      xpack.security.transport.ssl.certificate_authorities: /usr/share/elasticsearch/config/certs/ca.crt
      xpack.security.transport.ssl.verification_mode: certificate
      thread_pool.search.size: ${elasticsearch_queue_size}

  resources:
    requests:
      cpu: "1000m"
      memory: "2Gi"
    limits:
      cpu: "${elasticsearch_cpu_limit}"
      memory: "${elasticsearch_memory_limit}"
