output "grafana_admin_password" {
  value     = try(random_password.grafana_admin[0].result, "")
  sensitive = true
}

output "openfaas_admin_password" {
  value     = try(random_password.openfaas_admin_password[0].result, "")
  sensitive = true
}

output "elasticsearch_ca_cert" {
  value     = try(data.kubernetes_secret.elasticsearch_certificates[0].data["ca.crt"], "")
  sensitive = true
}
