variable "cluster_id" {
  description = "The ID of the Kubernetes cluster"
  type        = string
}

variable "cluster_domain" {
  description = "The domain name of the Kubernetes cluster"
  type        = string
}

variable "cluster_provider" {
  description = "The provider of the Kubernetes cluster"
  type        = string
}

variable "k8s_dashboard_enabled" {
  type        = bool
  description = "Whether to enable the Kubernetes dashboard"
  default     = false
}

variable "k8s_dashboard_host" {
  type        = string
  description = "The host name for the Kubernetes dashboard"
  default     = null
}

variable "lets_encrypt_notification_inbox" {
  type        = string
  description = "Email to send any email notifications about Letsencrypt"
}

variable "ingress_resource_quota" {
  type = object({
    limits   = optional(any)
    requests = optional(any)
  })
  description = "Resource configuration for the ingress controller"
  default     = {}
}

variable "prometheus_enabled" {
  type        = bool
  description = "Whether to enable Prometheus monitoring"
  default     = false
}

variable "prometheus_additional_alerts" {
  type        = string
  description = "Additional PrometheusRule alerts in YAML format."
  default     = ""
  validation {
    condition     = length(trimspace(var.prometheus_additional_alerts)) == 0 || can(yamldecode(var.prometheus_additional_alerts))
    error_message = "The additional_prometheus_alerts variable must be valid YAML or an empty string."
  }
}

variable "grafana_enabled" {
  type        = bool
  description = "Whether to enable Grafana monitoring"
  default     = false
}

variable "grafana_host" {
  type        = string
  description = "The host name for Grafana"
  default     = null
}

variable "alertmanager_enabled" {
  type        = bool
  description = "Whether to enable Alertmanager monitoring"
  default     = false
}

variable "alertmanager_config" {
  type        = string
  description = "The alertmanager configuration"
}

variable "velero_enabled" {
  type        = bool
  description = "Whether to enable Velero backup"
  default     = false
}

variable "velero_backup_bucket" {
  type        = string
  description = "The name of the bucket to store Velero backups"
  default     = null
}

variable "velero_backup_region" {
  type        = string
  description = "The region of the bucket to store Velero backups"
  default     = null
}

variable "velero_backup_access_key_id" {
  type        = string
  description = "The access key ID for the bucket to store Velero backups"
  default     = null
}

variable "velero_backup_secret_access_key" {
  type        = string
  description = "The secret access key for the bucket to store Velero backups"
  default     = null
}

variable "velero_volume_snapshot_provider" {
  type        = string
  description = "The volume snapshot provider for Velero"
}

variable "velero_plugin_aws_version" {
  type        = string
  description = "The version of the Velero AWS plugin"
  default     = "v1.15.0"
}

variable "velero_plugin_digitalocean_version" {
  type        = string
  description = "The version of the Velero DigitalOcean plugin"
  default     = null
}

variable "velero_plugin_digitalocean_token" {
  type        = string
  description = "The DigitalOcean token for the Velero DigitalOcean plugin"
  default     = null
}

variable "openfaas_enabled" {
  type        = bool
  description = "Whether to enable OpenFAAS integration"
  default     = false
}

variable "openfaas_host" {
  type        = string
  description = "The host name for OpenFAAS Gateway"
  default     = null
}

variable "elasticsearch_enabled" {
  type        = bool
  description = "Whether to enable the shared Elasticsearch"
  default     = false
}

variable "elasticsearch_config" {
  type = object({
    heap_size         = optional(string)
    search_queue_size = optional(string)
    cpu_limit         = optional(string)
    memory_limit      = optional(string)

  })

  default = {}

  description = "ElasticSearch configuration."
}

variable "openedx_instances" {
  type = list(string)
  description = "List of open edX instances to install elasticsearch root CA for"
  default = []
}
