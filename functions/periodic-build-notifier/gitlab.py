import os
from datetime import datetime
from pathlib import Path
from typing import List
from urllib.parse import urljoin
from zipfile import ZipFile

import requests

from .utils import get_secret


def call_gitlab_api(path: str) -> requests.Response:
    """
    Call the GitLab API and return the response it returned.
    In case of a failed request, the function produces an exception.

    Args:
        path: API resource path

    Returns:
        The response object returned by the API
    """

    gitlab_url: str = os.getenv("GITLAB_BASE_URL")
    if not gitlab_url:
        raise ValueError("GitLab URL is not set")

    response = requests.get(
        urljoin(gitlab_url, path), headers={"PRIVATE-TOKEN": get_secret("gitlab-token")}
    )

    response.raise_for_status()
    return response


def get_failed_job_id(project_id: int, pipeline_id: int) -> int:
    """
    Get the job ID which caused the pipeline failure.

    GitLab API returns only those jobs that are in direct relation with the pipeline,
    those jobs which are created by a child pipeline are not returned. To return those
    jobs as well, we need to get all failed child pipelines first.

    To get the single failing job id, we are selecting the pipeline which failed first,
    assuming that the pipeline caused the issue and potential other failures. Then, the
    failed pipeline's jobs are retrieved and filtered for the earliest failure. The job
    retrieved this way has the highest potential of causing other failures too.

    When calculating the earliest pipeline failure, we don't take the microseconds into
    consideration. The root cause of two jobs failing simultaneously within one second
    would mean a YAML validation error.In that case, it doesn't really matter which
    failed first.

    Args:
        project_id: ID of the project which executed the pipeline
        pipeline_id: ID of the pipeline producing the failed job

    Returns:
        The ID of the job which failed the pipeline.
    """

    def filter_prohibited_failure(items: List[dict]) -> List[dict]:
        return [j for j in items if not j["allow_failure"]]

    def get_datetime_difference(item: dict) -> int:
        created_at = datetime.strptime(
            item["created_at"].split(".")[0], "%Y-%m-%dT%H:%M:%S"
        )

        updated_at = datetime.strptime(
            item["updated_at"].split(".")[0], "%Y-%m-%dT%H:%M:%S"
        )

        return (updated_at - created_at).seconds

    def get_duration_difference(item: dict) -> int:
        return item["duration"] - item["queued_duration"]

    api_base_path = f"/api/v4/projects/{project_id}/pipelines"

    child_pipelines_response = call_gitlab_api(
        f"{api_base_path}/{pipeline_id}/bridges?scope=failed"
    ).json()

    failed_child_pipelines = filter_prohibited_failure(child_pipelines_response)
    failed_downstream = [p["downstream_pipeline"] for p in failed_child_pipelines]

    # If no failed failed_downstream pipelines found, then the failing job was within
    # the original pipeline
    if len(failed_downstream) > 0:
        failed_pipeline_id = min(failed_downstream, key=get_datetime_difference)["id"]
    else:
        failed_pipeline_id = pipeline_id

    jobs_response = call_gitlab_api(
        f"{api_base_path}/{failed_pipeline_id}/jobs?scope=failed"
    ).json()

    failed_jobs = filter_prohibited_failure(jobs_response)
    failed_job = min(failed_jobs, key=get_duration_difference)

    return failed_job["id"]


def download_job_logs(project_id: int, job_id: int) -> (Path, bytes):
    """
    Download logs from GitLab and return its content.

    The log file may not attachable to the email due to file size limits,
    therefore compressing the logfile is expected.

    Args:
        project_id: GitLab project ID which executed the job
        job_id: GitLab job ID which produced the log file

    Returns:
        Returns the log lines returned by GitLab.
    """

    return call_gitlab_api(f"/api/v4/projects/{project_id}/jobs/{job_id}/trace").content
