import json
import os
import re
from typing import List

from .cluster import configure_client, get_failed_pods, get_pod_logs
from .gitlab import download_job_logs, get_failed_job_id
from .notification import send_email
from .utils import compress_content

PIPELINE_TRIGGER_KEYWORD_PATTERN: str = r"^\[AutoDeploy\]\[Update\]\ "


def extract_instance_names(commit_message: str) -> List[str]:
    """
    Extract instance names from commit messages.

    Args:
        commit_message: git commit message triggered the job

    Returns:
        List of instance names used for periodic builds.
    """

    return re.sub(PIPELINE_TRIGGER_KEYWORD_PATTERN, "", commit_message).split(",")


def has_request_error(payload: dict, commit_message: str) -> str:
    """
    Validates that the request meets the purpose of the function.

    Args:
        payload: GitLab webhook request
        commit_message: extracted and formatted commit message

    Returns:
        Error message or empty string as OpenFAAS functions are not handling logging
        well.
    """

    attributes = payload.get("object_attributes", {})

    # Something else triggered the function.
    if payload.get("object_kind") != "pipeline":
        return "object kind is not a pipeline"

    # Webhook attributes are not matching, therefore we don't need to execute
    # the function.
    if (
        attributes.get("status") != "failed"
        or attributes.get("source") != "parent_pipeline"
        or attributes.get("ref") != os.getenv("GITLAB_REPO_REF")
    ):
        return "webhook attributes are not matching"

    is_matching_commit = re.match(
        rf"{PIPELINE_TRIGGER_KEYWORD_PATTERN}[,\w\d-]+$", commit_message
    )

    # The triggered pipeline was not a deployment.
    if not is_matching_commit:
        return "commit message pattern is not matching"

    return ""


class Event:
    body: str
    query: dict


def handle(event: Event, _) -> str:
    """
    Handle any incoming pipeline webhooks.

    The function validates if the webhook can be processed. In the case of a
    valid request, the handler downloads, compresses and sends the log files
    to the given recipients.

    Args:
        req: HTTP request hit the function handler

    Returns:
        Status of the execution if no errors raised.
    """

    payload = json.loads(event.body)

    project = payload.get("project", {})
    object_attributes = payload.get("object_attributes", {})
    variables = object_attributes.get("variables", {})

    commit_message = next(
        (v["value"].strip() for v in variables if v["key"] == "COMMIT_MESSAGE"),
        "",
    )

    validation_error = has_request_error(payload, commit_message)
    if validation_error:
        return json.dumps({"message": validation_error})

    instances = extract_instance_names(commit_message)
    compression_content = []

    # Get GitLab build logs
    job_id = get_failed_job_id(project["id"], object_attributes["id"])
    build_logs = download_job_logs(project["id"], job_id)
    compression_content.append(("build_logs", build_logs))

    # Get Kubernetes pod logs
    client = configure_client()

    for instance in instances:
        failed_pods = get_failed_pods(client, namespace=instance)
        pod_logs = get_pod_logs(client, failed_pods)
        compression_content.extend(pod_logs)

    # Compress build and pod logs into a single zip file
    zip_path = compress_content(contents=compression_content)

    # Get relevant lines from build logs.
    relevant_lines = "\n".join(
        [line.decode("utf-8") for line in build_logs.split(b"\n")[-25:]]
    )

    for instance in instances:
        send_email(
            subject=f"Deployment failed for instance: {instance}",
            body=(
                f"The periodic deployment of {instance} failed. Please see the details "
                "below, and in the attachments.\n\n"
                f"Relevant log lines:\n\n<code>{relevant_lines}</code>"
            ),
            attachment=zip_path,
        )

    return json.dumps(
        {
            "message": "success",
            "affected_instances": len(instances),
            "instance_names": instances,
            "attachment_size": zip_path.stat().st_size,
        }
    )
