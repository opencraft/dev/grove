"""
Kubernetes module provides utility functions working with the k8s API.
"""

from datetime import datetime, timedelta, timezone

from kubernetes.client import ApiClient, Configuration, CoreV1Api, V1Pod
from .utils import get_secret, get_secret_path


class KubernetesClient:
    """
    Client for accessing the k8s API.
    """

    def __init__(self):
        self.api_client: CoreV1Api = self.__configure_client()

    def __configure_client(self) -> CoreV1Api:
        """
        Configure the Kubernetes client and return a core v1 API client for
        later use.

        Although returning the client wouldn't be necessary, since only v1
        resources are used in Grove it is safe to return the v1 API client.
        """
        # OpenFAAS expands the service account secret, therefore the data.token key is
        # available as token. The value is coming from periodic-build-notifier-access-token.
        conf = Configuration(
            host="https://kubernetes.default.svc",
            api_key={"authorization": get_secret("token")},
            api_key_prefix={"authorization": "Bearer"},
        )
        conf.ssl_ca_cert = get_secret_path("ca.crt")
        return CoreV1Api(api_client=ApiClient(conf))

    def get_namespaces(self) -> list[str]:
        """
        Use the k8s api to fetch a list of namespaces
        """
        namespaces = self.api_client.list_namespace().items
        return [ns.metadata.name for ns in namespaces]

    def get_pod_in_namespace(self, selector: str, namespace: str) -> V1Pod | None:
        """
        Use the k8s api to get the pod in a namespace.
        """
        pods = self.api_client.list_namespaced_pod(namespace, label_selector=selector)
        return next(iter(pods.items), None)
