from pr_watcher.conf import config


def test_digitalocean_config_get_spaces_endpoint():
    """
    Test that the `get_spaces_endpoint` method of the `DigitalOceanConfig`
    returns the formatted endpoint url.
    """

    cfg = config.digitalocean
    assert cfg.get_spaces_endpoint() == "https://nyc3.digitaloceanspaces.com"
