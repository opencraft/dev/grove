from ..openedx import (
    NAMED_RELEASE_LATEST_COMMON_VERSIONS,
    NAMED_RELEASE_TUTOR_REQUIREMENTS,
    NamedRelease,
    get_mfe_name,
)


def test_named_release_values():
    assert NamedRelease.values() == [
        NamedRelease.NUTMEG.value,
        NamedRelease.OLIVE.value,
        NamedRelease.PALM.value,
        NamedRelease.QUINCE.value,
        NamedRelease.MASTER.value,
        NamedRelease.UNKNOWN.value,
    ]


def test_named_release_release_name():
    for release in NamedRelease:
        expected_release_name = release.value.split(".")[0]
        assert release.release_name == expected_release_name


def test_named_release_tutor_requirements():
    for release in NamedRelease:
        expected_requirements = NAMED_RELEASE_TUTOR_REQUIREMENTS.get(
            release.value, NAMED_RELEASE_TUTOR_REQUIREMENTS[NamedRelease.MASTER.value]
        )
        assert release.tutor_requirements == expected_requirements


def test_named_release_latest_common_version():
    for release in NamedRelease:
        expected_version = NAMED_RELEASE_LATEST_COMMON_VERSIONS.get(
            release.value,
            NAMED_RELEASE_LATEST_COMMON_VERSIONS[NamedRelease.MASTER.value],
        )
        assert release.latest_common_version == expected_version


def test_get_mfe_name():
    # The repository name does not contain the mfe prefix
    assert get_mfe_name("test-mfe") == "test-mfe"
    assert get_mfe_name("test-mfe-1") == "test-mfe-1"
    assert get_mfe_name("test-mfe-1-2") == "test-mfe-1-2"

    # The repository name contains the mfe prefix
    assert get_mfe_name("frontend-app-mfe") == "mfe"
    assert get_mfe_name("frontend-app-mfe-1") == "mfe-1"
    assert get_mfe_name("frontend-app-mfe-1-2") == "mfe-1-2"
