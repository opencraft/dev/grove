import pytest

from datetime import datetime
from pathlib import PosixPath
from unittest.mock import patch, mock_open

from usage_reporter.utils import (
    get_report_directory,
    get_report_filename,
    get_secret,
    SECRETS_DIR,
    get_secret_path,
)

TEST_REPORT_DATE = datetime(2025, 1, 1, 23, 59, 59)


@pytest.mark.parametrize(
    "provider,month,is_monthly,expected",
    [
        ("prometheus", TEST_REPORT_DATE, True, "k8s-resource-usage-monthly/prometheus"),
        (
            "prometheus",
            TEST_REPORT_DATE,
            False,
            "k8s-resource-usage-daily/prometheus/202501",
        ),
        (
            "k8s-metrics",
            TEST_REPORT_DATE,
            True,
            "k8s-resource-usage-monthly/k8s-metrics",
        ),
        (
            "k8s-metrics",
            TEST_REPORT_DATE,
            False,
            "k8s-resource-usage-daily/k8s-metrics/202501",
        ),
    ],
)
def test_get_report_directory(provider, month, is_monthly, expected):
    """
    Test the report directory formatting contains the provider, the month, and
    the report frequency (daily/monthly).
    """
    assert get_report_directory(provider, month, is_monthly) == expected


@pytest.mark.parametrize(
    "provider,timestamp,is_monthly,expected",
    [
        (
            "prometheus",
            TEST_REPORT_DATE,
            True,
            "k8s-resource-usage-monthly/prometheus/20250101-235959.csv",
        ),
        (
            "prometheus",
            TEST_REPORT_DATE,
            False,
            "k8s-resource-usage-daily/prometheus/202501/20250101-235959.csv",
        ),
        (
            "k8s-metrics",
            TEST_REPORT_DATE,
            True,
            "k8s-resource-usage-monthly/k8s-metrics/20250101-235959.csv",
        ),
        (
            "k8s-metrics",
            TEST_REPORT_DATE,
            False,
            "k8s-resource-usage-daily/k8s-metrics/202501/20250101-235959.csv",
        ),
    ],
)
def test_get_report_filename(provider, timestamp, is_monthly, expected):
    """
    Test the report file formatting contains the directory name and timestamp.
    """
    assert get_report_filename(provider, timestamp, is_monthly) == expected


@pytest.mark.parametrize(
    "secret,content,expected",
    [
        ("secret-1", "content", "content"),
        ("secret-2", "  content  ", "content"),
        ("secret-3", "\ncontent\t", "content"),
        ("secret-3", "cont\nent", "cont\nent"),
    ],
)
def test_get_secret(secret, content, expected):
    """
    Test getting a mounted secret from the file system.
    """
    with patch("builtins.open", mock_open(read_data=content)) as mock_file:
        assert get_secret(secret) == expected

    mock_file.assert_called_with(PosixPath(f"{SECRETS_DIR}/{secret}"), "r")


def test_get_secret_path():
    """
    Test getting a secret path.
    """
    assert get_secret_path("secret-1") == PosixPath(f"{SECRETS_DIR}/secret-1")
