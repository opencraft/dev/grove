import pytest

from usage_reporter.namespace_metric import (
    NamespaceMetric,
    DEFAULT_VM_CPU_COUNT,
    DEFAULT_VM_MEMORY_MEGABYTES,
)


@pytest.mark.parametrize(
    "current,metric,expected,exception",
    [
        (
            NamespaceMetric(namespace="default", cpu_minutes=1.0, memory_megabytes=2.0),
            NamespaceMetric(namespace="default", cpu_minutes=1.0, memory_megabytes=2.0),
            NamespaceMetric(namespace="default", cpu_minutes=2.0, memory_megabytes=4.0),
            None,
        ),
        (
            NamespaceMetric(namespace="default", cpu_minutes=1.5, memory_megabytes=2.0),
            NamespaceMetric(namespace="default", cpu_minutes=1.5, memory_megabytes=0.5),
            NamespaceMetric(namespace="default", cpu_minutes=3.0, memory_megabytes=2.5),
            None,
        ),
        (
            NamespaceMetric(namespace="default", cpu_minutes=1.5, memory_megabytes=2.0),
            NamespaceMetric(
                namespace="instance-1", cpu_minutes=1.5, memory_megabytes=0.5
            ),
            NamespaceMetric(namespace="default", cpu_minutes=1.5, memory_megabytes=2.0),
            "the metric namespaces are mismatching",
        ),
    ],
)
def test_namespace_metric_combine(current, metric, expected, exception):
    """
    Test combining namespace metrics.
    """
    if exception:
        with pytest.raises(ValueError, match=exception):
            current.combine(metric)
        return

    assert current.combine(metric) == expected


@pytest.mark.parametrize(
    "data,expected",
    [
        (
            {
                "metadata": {"namespace": "default"},
                "containers": [{"usage": {"cpu": 0.001, "memory": 1024**2}}],
            },
            NamespaceMetric(namespace="default", cpu_minutes=1.0, memory_megabytes=1.0),
        ),
        (
            {
                "metadata": {"namespace": "instance-1"},
                "containers": [{"usage": {"cpu": 0.010, "memory": 10 * 1024**2}}],
            },
            NamespaceMetric(
                namespace="instance-1", cpu_minutes=10.0, memory_megabytes=10.0
            ),
        ),
        (
            {"metadata": {"namespace": "instance-2"}, "containers": []},
            NamespaceMetric(namespace="instance-2", cpu_minutes=0, memory_megabytes=0),
        ),
    ],
)
def test_namespace_metric_import_metric(data, expected):
    """
    Test importing namespace metric from dict.
    """
    assert NamespaceMetric.import_metric(data) == expected


@pytest.mark.parametrize(
    "metric,threshold_cpu,threshold_memory,expected",
    [
        (
            NamespaceMetric(namespace="default", cpu_minutes=100, memory_megabytes=1.0),
            DEFAULT_VM_CPU_COUNT,
            DEFAULT_VM_MEMORY_MEGABYTES,
            0.2,
        ),
        (
            NamespaceMetric(
                namespace="default", cpu_minutes=300, memory_megabytes=4864
            ),
            DEFAULT_VM_CPU_COUNT,
            DEFAULT_VM_MEMORY_MEGABYTES,
            1.0,
        ),
        (
            NamespaceMetric(
                namespace="default", cpu_minutes=500, memory_megabytes=4096
            ),
            DEFAULT_VM_CPU_COUNT,
            DEFAULT_VM_MEMORY_MEGABYTES,
            1.0,
        ),
        (
            NamespaceMetric(
                namespace="default", cpu_minutes=1000, memory_megabytes=4096
            ),
            DEFAULT_VM_CPU_COUNT,
            DEFAULT_VM_MEMORY_MEGABYTES,
            2.0,
        ),
        (
            NamespaceMetric(
                namespace="default", cpu_minutes=500, memory_megabytes=9728
            ),
            DEFAULT_VM_CPU_COUNT,
            DEFAULT_VM_MEMORY_MEGABYTES,
            2.0,
        ),
    ],
)
def test_namespace_metric_num_equivalent_vms(
    metric, threshold_cpu, threshold_memory, expected
):
    """
    Test calculating how many VMs would have been needed for a given metric
    when the thresholds are set.
    """
    assert metric.num_equivalent_vms(threshold_cpu, threshold_memory) == expected


def test_namespace_metric_asdict():
    """
    Test dictionary representation of the namespace metric.
    """
    metric = NamespaceMetric(
        namespace="default", cpu_minutes=500, memory_megabytes=9728
    )
    expected = {
        "namespace": "default",
        "cpu_minutes": 500,
        "memory_megabytes": 9728,
        "equivalent_vms": 2,
    }

    assert metric.asdict() == expected
