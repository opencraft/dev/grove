from unittest.mock import patch, Mock

from usage_reporter.k8s_metrics import fetch_namespace_metrics
from usage_reporter.namespace_metric import NamespaceMetric


@patch("usage_reporter.k8s_metrics.configure_client", autospec=True)
def test_fetch_namespace_metrics(mock_client):
    """
    Test fetching the namespace metrics and parsing them.
    """
    mock_api = Mock()
    mock_api.list_cluster_custom_object.return_value = {
        "items": [
            {
                "metadata": {"namespace": "instance-1"},
                "containers": [{"usage": {"cpu": 0.001, "memory": 1024**2}}],
            },
            {
                "metadata": {"namespace": "instance-2"},
                "containers": [{"usage": {"cpu": 0.002, "memory": 2048**2}}],
            },
        ]
    }

    mock_client.return_value = mock_api

    metrics = fetch_namespace_metrics(None)

    assert metrics == [
        NamespaceMetric(namespace="instance-1", cpu_minutes=1.0, memory_megabytes=1.0),
        NamespaceMetric(namespace="instance-2", cpu_minutes=2.0, memory_megabytes=4.0),
    ]

    mock_api.list_cluster_custom_object.assert_called_once_with(
        "metrics.k8s.io",
        "v1beta1",
        "pods",
    )
