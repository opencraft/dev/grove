import pytest

from datetime import datetime
from unittest.mock import Mock, patch

from usage_reporter.prometheus_metrics import (
    fetch_namespace_metrics,
    prometheus_api_request,
    fetch_cpu_metrics,
    fetch_memory_metrics,
)
from usage_reporter.namespace_metric import NamespaceMetric

TEST_PROMETHEUS_QUERY_URL = "https://example.com/test?query"


@patch("usage_reporter.prometheus_metrics.os", autospec=True)
@patch("usage_reporter.prometheus_metrics.requests", autospec=True)
@pytest.mark.parametrize(
    "promql,timestamp,query",
    [
        (
            "test-query",
            datetime(2025, 1, 1, 23, 59, 59),
            f"{TEST_PROMETHEUS_QUERY_URL}time=1735761599.0&query=test-query&step=5m",
        ),
        (
            "other-test-query",
            datetime(2024, 1, 1, 23, 59, 59),
            f"{TEST_PROMETHEUS_QUERY_URL}time=1704139199.0&query=other-test-query&step=5m",
        ),
    ],
)
def test_prometheus_api_request(mock_request, mock_os, promql, timestamp, query):
    """
    Test Prometheus query (promql) execution sent over the API.
    """
    mock_os.getenv.return_value = TEST_PROMETHEUS_QUERY_URL

    mock_request_response = Mock()
    mock_request_response.json.return_value = {
        "data": {
            "result": [
                {"metric": {"namespace": "default"}, "value": [0, 10]},
                {"metric": {"namespace": "instance-1"}, "value": [0, 20]},
                {"metric": {"namespace": "instance-2"}, "value": [0, 30]},
            ]
        }
    }
    mock_request.get.return_value = mock_request_response

    results = prometheus_api_request(promql, timestamp)

    mock_os.getenv.assert_called_once_with(
        "PROMETHEUS_HOST", "http://prometheus-operated.harmony.svc:9090/api/v1/query?"
    )
    mock_request.get.assert_called_once_with(query, timeout=60)
    assert results == {
        "default": 10.0,
        "instance-1": 20.0,
        "instance-2": 30.0,
    }


@patch("usage_reporter.prometheus_metrics.prometheus_api_request")
def test_fetch_cpu_metrics(mock_prometheus_api_request):
    """
    Test prometheus API query to get CPU usage.
    """
    timestamp = datetime(2025, 1, 1, 23, 59, 59)
    expected = {"expected": True}
    mock_prometheus_api_request.return_value = expected

    result = fetch_cpu_metrics(timestamp)

    mock_prometheus_api_request.assert_called_once_with(
        'sum(rate(container_cpu_usage_seconds_total{image!=""}[5m])) by (namespace)',
        timestamp,
    )
    assert result == expected


@patch("usage_reporter.prometheus_metrics.prometheus_api_request")
def test_fetch_memory_metrics(mock_prometheus_api_request):
    """
    Test prometheus API query to get memory usage.
    """
    timestamp = datetime(2025, 1, 1, 23, 59, 59)
    expected = {"expected": True}
    mock_prometheus_api_request.return_value = expected

    result = fetch_memory_metrics(timestamp)

    mock_prometheus_api_request.assert_called_once_with(
        'sum(container_memory_working_set_bytes{image!=""}) by (namespace)', timestamp
    )
    assert result == expected


@patch("usage_reporter.prometheus_metrics.fetch_cpu_metrics")
@patch("usage_reporter.prometheus_metrics.fetch_memory_metrics")
def test_fetch_namespace_metrics(mock_fetch_memory_metrics, mock_fetch_cpu_metrics):
    """
    Test fetching the CPU and memory usage for all namespaces.
    """
    mock_fetch_cpu_metrics.return_value = {
        "default": 10.0,
        "instance-1": 20.0,
        "instance-2": 30.0,
    }
    mock_fetch_memory_metrics.return_value = {
        "default": 5.0 * 1024**2,
        "instance-1": 10.0 * 1024**2,
        "instance-2": 15.0 * 1024**2,
    }
    timestamp = datetime(2025, 1, 1, 23, 59, 59)

    result = fetch_namespace_metrics(timestamp)

    mock_fetch_cpu_metrics.assert_called_once_with(timestamp)
    mock_fetch_memory_metrics.assert_called_once_with(timestamp)
    assert result == [
        NamespaceMetric(namespace="default", cpu_minutes=10000.0, memory_megabytes=5.0),
        NamespaceMetric(
            namespace="instance-1", cpu_minutes=20000.0, memory_megabytes=10.0
        ),
        NamespaceMetric(
            namespace="instance-2", cpu_minutes=30000.0, memory_megabytes=15.0
        ),
    ]
