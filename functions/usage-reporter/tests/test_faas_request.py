from datetime import datetime

import pytest

from usage_reporter import prometheus_metrics, k8s_metrics
from usage_reporter.faas_request import (
    RequestValueError,
    get_provider,
    get_num_months,
    get_timestamp,
)

TEST_TIMESTAMP = datetime(2025, 1, 1, 23, 59, 59)


@pytest.mark.parametrize(
    "data,expected,exception",
    [
        ({}, prometheus_metrics, None),  # Default provider
        ({"provider": "prometheus"}, prometheus_metrics, None),
        ({"provider": "k8s-metrics"}, k8s_metrics, None),
        ({"provider": "unknown"}, None, "invalid provider unknown"),
    ],
)
def test_get_provider(data, expected, exception):
    """
    Test retrieving the provider from the request data.
    """

    if exception is not None:
        with pytest.raises(RequestValueError, match=exception):
            get_provider(data)

        return

    assert get_provider(data) == expected


@pytest.mark.parametrize(
    "data,expected,exception",
    [
        ({}, None, None),
        ({"num_months": 1}, 1, None),
        ({"num_months": 1.4}, 1, None),
        ({"num_months": 1.9}, 1, None),
        ({"num_months": "1"}, 1, None),
        ({"num_months": "one"}, None, "invalid num_months provided one"),
    ],
)
def test_get_num_months(data, expected, exception):
    """
    Test the number of months is returned and parsed if found.
    """

    if exception is not None:
        with pytest.raises(RequestValueError, match=exception):
            get_num_months(data)

        return

    assert get_num_months(data) == expected


@pytest.mark.parametrize(
    "data,expected,exception",
    [
        ({"timestamp": "2025-01-01T23:59:59"}, TEST_TIMESTAMP, None),
        ({"timestamp": "now"}, None, "invalid timestamp provided now"),
    ],
)
def test_get_num_months(data, expected, exception):
    """
    Test the number of months is returned and parsed if found.
    """

    if exception is not None:
        with pytest.raises(RequestValueError, match=exception):
            get_timestamp(data)

        return

    assert get_timestamp(data) == expected
