from tempfile import mktemp
from unittest.mock import Mock

from usage_reporter.exporter import export_to_csv
from usage_reporter.namespace_metric import NamespaceMetric


def test_export_to_csv():
    """
    Test exporting metrics to a CSV file.
    """

    default_metric = NamespaceMetric("default", 0.3, 0.5)
    default_metric.num_equivalent_vms = Mock(return_value=0.5)

    instance_1_metric = NamespaceMetric("instance-1", 0.4, 0.6)
    instance_1_metric.num_equivalent_vms = Mock(return_value=0.5)

    instance_2_metric = NamespaceMetric("instance-2", 0.5, 0.7)
    instance_2_metric.num_equivalent_vms = Mock(return_value=0.5)

    instance_3_metric = NamespaceMetric("instance-3", 0.6, 0.8)
    instance_3_metric.num_equivalent_vms = Mock(return_value=0.5)

    metrics: list[NamespaceMetric] = [
        default_metric,
        instance_1_metric,
        instance_2_metric,
        instance_3_metric,
    ]

    csv_file = mktemp()

    with open(csv_file, "w") as f:
        export_to_csv(f, metrics)

    with open(csv_file, "r") as f:
        assert f.readlines() == [
            "Namespace,CPU,Memory,Equivalent VMs\n",
            "default,0.3,0.5,0.5\n",
            "instance-1,0.4,0.6,0.5\n",
            "instance-2,0.5,0.7,0.5\n",
            "instance-3,0.6,0.8,0.5\n",
        ]
