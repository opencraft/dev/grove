"""
Module to store the NamespaceMetric dataclass.
"""

from dataclasses import dataclass

from kubernetes.utils.quantity import parse_quantity

DEFAULT_VM_CPU_COUNT: float = 0.5
DEFAULT_VM_MEMORY_MEGABYTES: int = int(round(1024 * 4.75, 0))


@dataclass
class NamespaceMetric:
    """
    Helper class to work with the metrics API. Provides attributes
    and methods to retrieve and store data received from the
    metrics api.
    """

    namespace: str
    cpu_minutes: float
    memory_megabytes: float

    def __assert_namespace(self, namespace: str):
        """
        Check if the namespace matches the current metrics' namespace.
        """
        if self.namespace != namespace:
            raise ValueError("the metric namespaces are mismatching")

    def combine(self, namespace_metric: "NamespaceMetric") -> "NamespaceMetric":
        """
        Adds the cpu_minutes and memory_megabytes of the namespace_metric
        to the existing values.
        """
        self.__assert_namespace(namespace_metric.namespace)
        self.cpu_minutes += namespace_metric.cpu_minutes
        self.memory_megabytes += namespace_metric.memory_megabytes
        return self

    @classmethod
    def import_metric(cls, metric: dict) -> "NamespaceMetric":
        """
        Parses an item from the k8s metrics api and returns
        a new instance of this class with the retrieved values.
        """
        cpu_usage = sum(
            parse_quantity(container["usage"]["cpu"])
            for container in metric["containers"]
        )
        mem_usage = sum(
            parse_quantity(container["usage"]["memory"])
            for container in metric["containers"]
        )
        return NamespaceMetric(
            cpu_minutes=float(cpu_usage * 1000),
            memory_megabytes=float(mem_usage / 1024 / 1024),
            namespace=metric["metadata"]["namespace"],
        )

    def num_equivalent_vms(
        self,
        vm_cpu_count: float = DEFAULT_VM_CPU_COUNT,
        vm_memory_megabytes: int = DEFAULT_VM_MEMORY_MEGABYTES,
    ) -> float:
        """
        Looks at the values stored to determine the number
        of VMs it would be equivalent to if using a VPS
        provider.
        """
        num_cpus = self.cpu_minutes / 1000
        num_vm_cpus_required = num_cpus / vm_cpu_count
        num_vms_required_for_mem = self.memory_megabytes / vm_memory_megabytes
        return max([num_vm_cpus_required, num_vms_required_for_mem])

    def asdict(self) -> dict:
        """
        Returns a dict representation of the class for exporting
        as json.
        """
        return {
            "namespace": self.namespace,
            "cpu_minutes": self.cpu_minutes,
            "memory_megabytes": self.memory_megabytes,
            "equivalent_vms": self.num_equivalent_vms(),
        }
