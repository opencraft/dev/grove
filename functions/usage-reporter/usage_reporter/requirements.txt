requests==2.32.2
kubernetes==32.0.0
boto3==1.36.12
types-boto3[essential]
python-dateutil==2.9.0.post0
