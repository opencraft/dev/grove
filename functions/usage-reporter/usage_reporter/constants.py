"""
Constants used in different places.
"""

from enum import StrEnum

REPORT_DATE_FORMAT = "%Y%m%d-%H%M%S"


class ReportType(StrEnum):
    """Available report types."""

    single = "single"
    aggregated = "aggregated"
