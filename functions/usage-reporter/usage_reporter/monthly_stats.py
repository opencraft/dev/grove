"""
Module for working with monthly metrics, like exporting average metrics.
"""

import csv
import datetime
import io
import os
from concurrent.futures import ThreadPoolExecutor

from dateutil.relativedelta import relativedelta
from dateutil.rrule import DAILY, rrule

from . import constants, utils
from .namespace_metric import NamespaceMetric
from .s3 import s3_client


def _csv_file_to_metric(s3_object) -> dict[str, NamespaceMetric]:
    """
    Reads a CSV file from S3 and return a list of `NamespaceMetric`
    instances for each line read.
    """
    reader = csv.DictReader(io.StringIO(s3_object.get()["Body"].read().decode("utf8")))

    metrics = {}
    for line in reader:
        metrics[line["Namespace"]] = NamespaceMetric(
            namespace=line["Namespace"],
            cpu_minutes=float(line["CPU"]),
            memory_megabytes=float(line["Memory"]),
        )
    return metrics


def _get_report_filenames(
    provider_name: str, num_months: int, start_date: datetime.datetime
) -> list:
    """
    Returns a list of S3 Resource Objects where the filename
    is within the time `end_date` - num_months.
    """
    # Swap the start and end_date, so that logic below still works
    if num_months < 0:
        end_date = start_date
        start_date = end_date + relativedelta(months=num_months)
    else:
        end_date = start_date + relativedelta(months=num_months)

    directories = set()
    for report_day in rrule(dtstart=start_date, until=end_date, freq=DAILY):
        report_directory = utils.get_report_directory(provider_name, report_day)
        directories.add(report_directory)

    valid_s3_objects = []

    for directory in directories:
        for s3_object in s3_client.list_directory(directory):
            report_date_string = os.path.splitext(os.path.basename(s3_object.key))[0]
            if not report_date_string:
                continue
            report_date = datetime.datetime.strptime(
                report_date_string, constants.REPORT_DATE_FORMAT
            )
            if end_date >= report_date >= start_date:
                valid_s3_objects.append(s3_object)

    return valid_s3_objects


def fetch_average_metrics(
    provider_name: str, num_months: int, end_date: datetime.datetime
) -> list:
    """
    Loops through all the metrics within the timespan `end_date - num_months`
    and returns the average for each namespace.
    """
    valid_s3_objects = _get_report_filenames(provider_name, num_months, end_date)

    executor = ThreadPoolExecutor()

    avg_metrics = {}
    for csv_metrics in executor.map(_csv_file_to_metric, valid_s3_objects):
        for namespace, metric in csv_metrics.items():
            if namespace not in avg_metrics:
                avg_metrics[namespace] = metric
            else:
                avg_metrics[namespace].combine(metric)

    for metric in avg_metrics.values():
        metric.cpu_minutes = metric.cpu_minutes / len(valid_s3_objects)
        metric.memory_megabytes = metric.memory_megabytes / len(valid_s3_objects)

    return list(avg_metrics.values())
