import smtplib
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import formatdate
from email import encoders


def compose_email(
    subject: str, message: str, files: list[str | Path] = None
) -> MIMEMultipart:
    """Compose email with optional attachments."""
    msg = MIMEMultipart()
    msg["Date"] = formatdate(localtime=True)
    msg["Subject"] = subject
    msg.attach(MIMEText(message))

    for file in map(lambda f: Path(f), files or []):
        part = MIMEBase("application", "octet-stream")
        part.set_payload(file.read_text())
        part.add_header("Content-Disposition", f"attachment; filename={file.name}")
        encoders.encode_base64(part)
        msg.attach(part)

    return msg


def send_mail(
    sender: str,
    recipients: list[str],
    msg: MIMEMultipart,
    server: str,
    username: str,
    password: str,
    port=587,
):
    """Open an SMTP connection and send the given message."""
    msg["From"] = sender
    msg["To"] = ", ".join(recipients)

    smtp = smtplib.SMTP(server, port)
    smtp.starttls()
    smtp.login(username, password)
    smtp.sendmail(sender, recipients, msg.as_string())
    smtp.quit()
