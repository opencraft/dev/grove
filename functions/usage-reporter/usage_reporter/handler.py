"""
OpenFaas hooks.
"""

import datetime
import json
import os
from tempfile import NamedTemporaryFile

from . import faas_request, monthly_stats, utils
from .constants import ReportType
from .exporter import export_to_csv
from .s3 import s3_client
from .smtp import compose_email, send_mail
from .utils import get_secret


class Event:
    body: str
    query: dict


def handle(event: Event, _) -> str:
    """
    Handler function for OpenFaas. Generated the usage report and
    uploads to S3.

    Return a JSON string with attributes:
      message: str -> "success" if report was generated successfully.
      url: str -> The S3 URL to the generated report.
    """

    s3_client.init_s3_api()
    data = json.loads(event.body) if event.body else {}

    try:
        provider = faas_request.get_provider(data)
        timestamp = faas_request.get_timestamp(data)
        num_months = faas_request.get_num_months(data)
    except faas_request.RequestValueError as request_error:
        return json.dumps({"message": str(request_error)})

    # If the aggregated report type was requested, but the number of months is
    # not set, override it to 1, meaning the aggregation of the past month.
    report_type = ReportType(os.getenv("REPORT_TYPE", ReportType.single.value))
    if num_months is None and report_type == ReportType.aggregated:
        num_months = -1

    report_items = (
        monthly_stats.fetch_average_metrics(provider.NAME, num_months, timestamp)
        if num_months is not None
        else provider.fetch_namespace_metrics(timestamp)
    )

    filename = utils.get_report_filename(
        provider.NAME, datetime.datetime.now(), bool(num_months)
    )

    with NamedTemporaryFile(mode="w+", encoding="utf8", suffix=".csv") as file:
        export_to_csv(file, report_items)
        object_url = s3_client.upload_to_bucket(filename, file)

        # If an email address is set, a copy of the CSV report will be sent
        recipients = list(
            filter(lambda x: x, os.getenv("EMAIL_TO", "").replace(" ", "").split(","))
        )
        if len(recipients) > 0:
            message = compose_email(
                subject=f"Aggregated usage report for the past {num_months} month(s)",
                message=f"Hello!\n\nA new aggregated usage report has been published.\nPlease find the aggregated usage report for the past {num_months} month(s) in the attachments.",
                files=[file.name],
            )

            send_mail(
                sender=os.getenv("EMAIL_FROM", ""),
                recipients=recipients,
                msg=message,
                username=get_secret("usage-report-smtp-username"),
                password=get_secret("usage-report-smtp-password"),
                server=os.getenv("SMTP_SERVER", ""),
                port=int(os.getenv("SMTP_PORT", 0)),
            )

    return json.dumps(
        {
            "message": "success",
            "report_url": object_url,
            "items": [z.asdict() for z in report_items],
        }
    )
