import datetime
import types
from typing import Optional

from . import k8s_metrics, prometheus_metrics

PROVIDERS = {
    "prometheus": prometheus_metrics,
    "k8s-metrics": k8s_metrics,
}


class RequestValueError(ValueError):
    """
    Except class to use when there's an issue with parsing the
    http request data.
    """


def get_provider(request_data: dict) -> types.ModuleType:
    """
    Checks if a `provider` key has been defined `request_data`
    and returns the metrics module if supported.

    Allowed provider values are `prometheus` and `k8s-metrics`.

    Defaults to `prometheus` if not provider.
    """
    provider_name = request_data.get("provider", "prometheus")
    if provider_name not in PROVIDERS:
        raise RequestValueError(f"invalid provider {provider_name}")

    return PROVIDERS[provider_name]


def get_num_months(request_data: dict) -> Optional[int]:
    """
    Checks if `num_months` is part of the request
    data and returns the integer value if provided.
    """
    num_months = request_data.get("num_months")

    if num_months is None:
        return None

    try:
        return int(num_months)
    except ValueError as exc:
        raise RequestValueError(f"invalid num_months provided {num_months}") from exc


def get_timestamp(request_data: dict) -> datetime.datetime:
    """
    Parse the `timestamp` key in request_data and returns
    the datetime objects.

    timestamp has to be in iso8601 format.
    """
    timestamp = request_data.get("timestamp")
    if timestamp is None:
        return datetime.datetime.now()

    try:
        return datetime.datetime.fromisoformat(timestamp)
    except ValueError as exc:
        raise RequestValueError(f"invalid timestamp provided {timestamp}") from exc
