# DigitalOcean

Grove DigitalOcean support is under active development.

## DigitalOcean Resources

Grove uses the following DigitalOcean resources for Open edX deployment:

### DigitalOcean DOKS

Grove uses a managed DOKS cluster to deploy Tutor instances in Kubernetes.

### Spaces storage

Grove uses one Spaces bucket for the Tutor env directory for each Open edX instances. Using multiple buckets would result in a `# of instances * $5` cost.

### DigitalOcean Managed MySQL

Grove uses shared MySQL as the database for each Open edX instance. Grove creates a root credential using terraform, and Tutor uses that to create instance-specific user and database. The database name is generated from the instance name, replacing all hyphhens (`-`) with underscores (`_`), then, adding the `_openedx` suffix. For example, database name for `test-instance` will be `test_instance_openedx`. Due to this, there can't be two instances with names `test-instance` and `test_instance` in the cluster. In case the length of database name is longer than 64 characters, it will take the first 64 characters as the database name (max length of MySQL database name can be 64 characters).

### DigitalOcean Managed MongoDB

Grove uses a shared MongoDB cluster for the Open edX instances. Each instance has read-write access to a database with the same name as the grove instance, as well as a dedicated discussion forum database, named as `{instance}-cs_comment_service`.
