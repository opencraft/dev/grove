# Infrastructure Destroy Pipeline

The Infrastructure Destroy Pipeline deletes all resources provisioned with terraform, including the Kubernetes cluster.

## Use case

Infrastructure can be destroyed [locally](/user-guides/working-locally), though that requires more setup and depends on the user's machine. To avoid this dependency and fully automate cluster creation, you can use the `[AutoDeploy][Infrastructure][Destroy] ...` commit message.

## Commit message

The following commit message pattern is used to trigger infrastructure destroy.

`[AutoDeploy][Infrastructure][Destroy] <ANY COMMIT MESSAGE>`

This commit message does not require any specific format following the `[AutoDeploy][Infrastructure][Destroy]` prefix.

Example commit message:

`[AutoDeploy][Infrastructure][Destroy] Remove cluster`
