# Working with OCIM

The [OpenCraft Instance Manager](https://gitlab.com/opencraft/dev/console/) used to be OpenCraft's main deployment tool for many years for edX instances. However, that is now replaced by Grove.

This transition is not a one-step move on OpenCraft's side, therefore Grove is integrated into OCIM. This integration is not complete due to many factors.

This guide is meant to document the caveats in the integration. Why documenting this in Grove? Since the integration requires more setup on Grove's side, it seemed to be more appropriate.

## Setting up cluster repositories

To create and deploy instances for a cluster repository, make sure to register the cluster repository first in OCIM. To do so, do the following

1. Visit the `/admin/grove/groveclusterrepository/` page
2. Click on "add grove cluster repository"
3. Fill the details and click "Save"
4. In the GitLab repository, navigate to settings -> webhooks
5. Add `https://<YOUR DOMAIN>/api/v1/grove/webhook/` for URL and check `Pipeline events`
6. Click save to add new webhook

Now, the cluster repository is ready to be used. You can create and deploy instances for the cluster repo.

## Creating new instances on Grove clusters

After setting up a Grove cluster repository, you can create new instances from OCIM. Creating new instances requires you to access OCIM shell.

1. Open an OCIM shell
2. Do `from instance.factories import production_instance_factory`
3. Run `production_instance_factory(name="YourInstanceName", sub_domain="instance-sub-domain")`

At this point a new, regular, OpenStack instance is created, though not deployed.

## Deploying instances to a Grove cluster

1. On the UI, find your instance and click on "Convert to Grove instance" (if it is not converted yet)
2. A new "Trigger new deployment" appears under the "Grove deployments" tab of the instance

Clicking the button triggers a new deployment. When the pipeline triggers, GitLab will report the status back to OCIM. The status of the deployment in OCIM is updated by the status change of the pipeline in GitLab.
