# Tear everything down

If you've just been testing Grove and you want to shut it down to avoid further charges from your cloud provider, it's easy to destroy everything. Obviously, only run this if you're sure you want to destroy everything - this cannot be undone!

## AWS

1. Log in to the console.
1. Find the S3 bucket called `tenv-....` and delete all of its files. (Terraform can't delete S3 buckets if they contain files.)
1. Go to EC2 load balancers, and delete any load balancers pointing into the Kubernetes cluster.

## DigitalOcean

1. Login to the UI.
1. Go to Spaces, find bucket created for your project, and delete the top-level folder. (Terraform can't delete Spaces buckets if they contain files.)
1. Go to Networking > VPC, and find the VPC created for your project.
    - Click on Resources and delete the load balancer attached to the VPC. (Terraform cannot delete the VPC if it has members.)
    - If the VPC is marked as "default", create a new VPC and make that the default. (Terraform cannot delete the default VPC.)

## Next steps

1. Make sure you are set up to work with this locally (see [Working Locally](./working-locally.md)).
1. Change into the `control` directory, e.g. `cd my-cluster/control/`
1. Run `./tf destroy` . It may take quite a long time.
