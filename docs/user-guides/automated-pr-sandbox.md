# Automated PR Sandbox

!!! note

    Settings used for upstream PRs to the repositories under the [openedx](https://github.com/openedx) org, can be found in [Upstream Open edX Settings](#upstream-open-edx-settings) section below.
    Settings used for OpenCraft's internal/upstream PRs can be found in [OpenCraft Internal/Upstream Settings](#opencraft-internalupstream-settings) section below.

The `pr_watcher` [OpenFAAS](https://grove.opencraft.com/addons/openfaas/) function in Grove automatically provisions sandboxes for PRs created by authorized users in the configured repositories. This function is triggered periodically every 15 minutes.

Here are some of the things this function does:

1. Periodically checks for new PRs created with the correct label by authorized users in watched repos and provisions sandboxes for them.
2. Updates PR sandboxes if changes are detected in the PR.
3. Notifies the status of sandbox provisioning as comments on the PR.
4. Enables PR authors to configure their sandbox using the PR description.
5. Automatically destroys the sandbox when a PR is closed, merged, or if no changes are detected in an open PR for an [extended period of time](#destroying-idle-sandboxes).

This guide is intended to help PR authors understand the automated sandbox workflow and show how to configure and interact with the sandbox and with the `pr_watcher` function.

## Watched Repositories

There are two ways to configure watched repositories with this function:

1. Forks can be configured dynamically using the pre-defined topic `pr-sandboxes-enabled` added to those repos that need to be watched for a given set of authorized Github orgs. The `GITHUB_WATCHED_ORGANIZATIONS` variable is used to configure the authorized Github orgs.
2. Individual forks can be configured directly with the function during deployment of the `pr_watcher` function using the `OPENEDX_WATCHED_FORKS` variable.

We differentiate two different categories of forks that can be configured with the `pr_watcher` function:

1. Forks of [edx-platform](https://github.com/openedx/edx-platform) repo. Sandboxes for PRs from these forks have the deployment branch configured as source repo/branch of the PR.
2. Forks for MFEs. Sandboxes for PRs of these forks have the `edx-platform` [master](https://github.com/openedx/edx-platform) branch configured as the deployment branch by default unless [overridden](#custom-configuration) by the PR author. The source repo/branch of the PR is configured as a [custom MFE](https://github.com/overhangio/tutor-mfe?tab=readme-ov-file#adding-new-mfes).

!!! warning

    For MFE forks it's important that the repo name starts with `frontend-app-`. Otherwise, the function will treat it as an `edx-platform` fork. This assumption is based on the naming convention of MFEs.

## Authorized Users and Team

To prevent abuse of resources, only PRs from authorized users are considered for sandbox creation. Authorized users can either be configured individually or through Github teams. All users belonging to these authorized teams would be treated as authorized users. By using the GitHub teams as the source of users, the user management can be dynamic.

## Enable Sandbox for PRs

Automatic sandbox management is enabled for a PR by adding the `create-sandbox` label to it. The PR also needs to be in a [watched repository](#watched-repositories) and by an [authorized user](#authorized-users-and-team).

## Sandbox Configuration

### Default Configurations

Based on the target branch of a PR, the `pr_watcher` function tries to determine which named release of Open edX the PR is intended for. Certain things such as `OPENEDX_COMMON_VERSION` and the release version of Tutor and its plugins are configured based on this.

For a PR to be recognized as targting a named release, its target branch should be of the format

    <release-prefix>/<named_release>.<version>

`open-release/palm.master`, `opencraft-release/palm.1` are examples of valid names of target branches.

!!! note

    Recognized named releases are limited to `nutmeg` and above.

If a named release cannot be determined from the target branch, then `OPENEDX_COMMON_VERSION` is set to `master` and the `main` version of Tutor and it's plugins are used. This assumption is applied in order to cover the vast majority of PRs. The `OPENEDX_COMMON_VERSION` can be [overridden](#1-tutor-configurations) by the PR author.

### Custom Configuration

PR authors can either override existing default configurations or set other configurations values for their sandbox directly from the PR body.

The custom configurations are of two different types:

#### 1. Tutor Configurations

Configurations passed directly to tutor and its plugins, such as `EDX_PLATFORM_REPOSITORY`, `OPENEDX_EXTRA_PIP_REQUIREMENTS`, `SITE_CONFIG` etc.

These configuration settings need to be added to the PR body in the following format:

    **Settings**

    ```yaml
    EDX_PLATFORM_REPOSITORY: https://github.com/openedx/edx-platform.git
    EDX_PLATFORM_VERSION: master

    OPENEDX_EXTRA_PIP_REQUIREMENTS:
    - dnspython
    - openedx-scorm-xblock<13.0.0,>=12.0.0

    SITE_CONFIG:
      version: 0
    ```

#### 2. Grove Configurations

These configuration settings are used by Grove itself to configure the sandbox instances, such as `TUTOR_GROVE_COMMON_SETTINGS`, `TUTOR_GROVE_COMMON_ENV_FEATURES`, `TUTOR_GROVE_WAFFLE_FLAGS`, etc. The plugin configuration is described in [Grove's Tutor plugin](https://gitlab.com/opencraft/dev/tutor-contrib-grove).

Similar to tutor configurations, these can be added to the PR body under `Settings` and can be mixed with other tutor configurations. Example:

    **Settings**

    ```yaml
    EDX_PLATFORM_REPOSITORY: https://github.com/openedx/edx-platform.git
    EDX_PLATFORM_VERSION: master

    TUTOR_GROVE_COMMON_ENV_FEATURES: |
      ASSUME_ZERO_GRADE_IF_ABSENT_FOR_ALL_TESTS: true

    TUTOR_GROVE_LMS_ENV: |
      REGISTRATION_VALIDATION_RATELIMIT: "100/s"
      REGISTRATION_RATELIMIT: "100/s"
      RATELIMIT_RATE: "100/s"
    ```

#### 3. Requirements

These are used to set the release-version/branch of Tutor and it's plugins to be used with the sandbox. Although the requirements below are necessary for the PR sandboxes, the PR author could add any other tutor plugins that is needed for the given PR. For example, if a new plugin is developed.

These can be added to the PR body in the following format:

    **Tutor requirements**

    ```txt
    git+https://github.com/overhangio/tutor.git@main
    git+https://github.com/overhangio/tutor-discovery.git@main
    git+https://github.com/overhangio/tutor-ecommerce.git@main
    git+https://github.com/overhangio/tutor-mfe.git@main
    git+https://github.com/overhangio/tutor-xqueue.git@main
    git+https://github.com/overhangio/tutor-forum.git@main
    git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@main
    git+https://github.com/hastexo/tutor-contrib-s3.git@main
    ```

!!! note
    To remove a config value from a sandbox simply set it to a blank or null value. Removing the config completely from the PR body will still cause it to persist in the sandbox.

    Ex. `TUTOR_GROVE_COMMON_ENV_FEATURES: ""`

### Notifications

The `pr_watcher` function posts notifications as comments in the PR to notify PR authors about the success/failure status of the sandbox :

1. If deployment succeeds, then the LMS URL, Studio URL, current instance configs and current instance requirements are posted along with the success notification
2. If deployment fails, then the failure logs, current instance configs and current instance requirements are posted with the failure notification.

### Destroying IDLE Sandboxes

The sandboxes, especially if the PRs are lasting for many weeks, can consume huge amount of resources. This is mitigated by destroying the sandboxes after a period of inactivity. This period is set to 14 days by default.

This is configurable using the `PR_WATCHER_SANDBOX_DESTROY_CUTOFF_DAYS` environment variable when deploying the `pr_watcher` function.

!!! note
    The automatic destroy cannot be turned off or extended per sandbox, nor can it be set from the PR description.

## Settings for some Use Cases

### Upstream Open edX Settings

- [GITHUB_WATCHED_ORGANIZATIONS](#watched-repositories) variable is set to `"openedx"`.
- [GITHUB_AUTHORIZED_TEAMS](#authorized-users-and-team) variable is set to `"openedx/openedx-pr-sandbox-creators"`

### OpenCraft Internal/Upstream Settings

- [GITHUB_WATCHED_ORGANIZATIONS](#watched-repositories) variable is set to `"openedx","open-craft"`.
- [GITHUB_AUTHORIZED_TEAMS](#authorized-users-and-team) variable is set to `"open-craft/core","open-craft/new-members-candidates"`
