# Private EdX Plugins

This guide describes how to add a plugin to your Open edX instance from a private git repository.

## Prerequisites

1. You have a working Open edX instance.
2. You have a private git repository with your plugin.
3. You have a user with SSH access to the private git repository.

## Configuration

Adding plugins from public repository is supported by default, using the `OPENEDX_EXTRA_PIP_REQUIREMENTS` key in the `config.yml` of the instance. However, that config is not capable of adding plugins from private repositories.

To support private repositories, Tutor provides a [manual configuration option](https://docs.tutor.overhang.io/configuration.html#installing-extra-requirements-from-private-repositories). Since that workflow does not fit well in Grove's workflow, we provide a simpler alternative.

Starting from Grove 0.8.0, you can add a plugin to your Open edX instance from a private git repository. To do so, you need to add the following key to the `grove.yml` of your instance (note that we are __not__ using the `config.yml`):

```yaml
OPENEDX_PRIVATE_REQUIREMENTS:
  - git@github.com:me/myprivaterepo.git

PRIVATE_SSH_KEY: |
    -----BEGIN RSA PRIVATE KEY-----
    ...
    -----END RSA PRIVATE KEY-----
```

The `OPENEDX_PRIVATE_REQUIREMENTS` defines the list of private git repositories to clone. The `PRIVATE_SSH_KEY` defines the SSH key to use to clone the repositories.

After adding the above configuration to your `grove.yml`, you can trigger a usual instance update by committing the changes with the appropriate `[AutoDeploy][Update] <INSTANCE_NAME>|<DEPLOY ID>` commit message.
