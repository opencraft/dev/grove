# Custom Scripts

Grove can add custom scripts to the Docker container at build time. Upon need, these scripts can be executed periodically or in a one-time manner, depending on the use-case.

These custom scripts are tied to instances, meaning that every instance has its separate custom scripts. To add customs scripts, create a `scripts` directory in the instance's configuration directory, like: `my-cluster/instances/my-instance/scripts`, and put any executable scripts inside.

Then, Grove copies over the directory and its content to pack with the Docker image built for the `openedx` image.

## Running scripts manually

The scripts will live in all docker containers built by Tutor from the `openedx` image. This means that to run scripts manually, one needs to get shell access first.

To do so, use the `./kubectl -n <instance namespace> exec -it <pod> /grove/scripts/<script>`.

## Running scripts periodically

To run scripts periodically, you may use the builtin [cron jobs](https://gitlab.com/opencraft/dev/tutor-contrib-grove/-/blob/c728d12c/tutorgrove/plugin.py#L173-188) of the `tutor-contrib-grove` package.
