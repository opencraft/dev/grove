# Enable OpenFAAS

Grove supports OpenFAAS, a function as a service software, deployed on the Kubernetes cluster.

The OpenFAAS installation is used for sending filtered periodic build failure notifications, though it may be used for other purposes by users of Grove.

## Configuration

The OpenFAAS installation requires `TF_VAR_openfaas_enabled` to be set to `"true"` in the `cluster.yml` file. Otherwise, the installation is turned off and resources won't be installed.

## Accessing the functions UI

OpenFAAS (community edition) comes with a basic UI for managing the functions installed.

![OpenFAAS UI](../images/OpenFAAS-UI.png)

If the `TF_VAR_openfaas_enabled` is set to `"true"` and changes are applied, the UI is available at `https://openfaas.<CLUSTER_DOMAIN>`. The installation is protected by basic auth. The username is `admin`, but the password is generated automatically. To retrieve the password, within the `control` directory, run `./tf output openfaas_admin_password`.
