# include tools-container variables
include:
  - tools-container/ci_vars.yml

stages:
  - tools-container-build
  - tools-container-tag
  - test
  - deploy

.tools-container:
  image: docker:24-dind
  services:
    - docker:24-dind
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE/$TOOLS_CONTAINER_IMAGE_NAME
    DEFAULT_TAG: $CI_COMMIT_REF_SLUG
    LATEST_TAG: latest
    VERSIONED_TAG: $TOOLS_CONTAINER_IMAGE_VERSION
    DOCKER_BUILDKIT: 1
    DOCKER_TLS_CERTDIR: "/certs"

build tools-container image:
  extends: .tools-container
  stage: tools-container-build
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - cd tools-container
    - docker build --build-arg BUILDKIT_INLINE_CACHE=1 --build-arg TOFU_VERSION="${TOFU_VERSION}" --build-arg GITLAB_TOFU_IMAGES_VERSION="${GITLAB_TOFU_IMAGES_VERSION}" --build-arg KUBECTL_VERSION="${KUBECTL_VERSION}" --build-arg HELM_VERSION="${HELM_VERSION}" --build-arg VELERO_VERSION="${VELERO_VERSION}" --cache-from $IMAGE_NAME:$LATEST_TAG -t $IMAGE_NAME:$DEFAULT_TAG .
    - docker push $IMAGE_NAME:$DEFAULT_TAG
  when: manual

tag tools-container image:
  extends: .tools-container
  stage: tools-container-tag
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - test "$(timeout -s SIGKILL 600 docker pull $IMAGE_NAME:$VERSIONED_TAG)" && echo "Image version $VERSIONED_TAG already exists. Please bump up the version by increasing the TOOLS_CONTAINER_IMAGE_VERSION variable." && exit 1 || true
    - docker pull $IMAGE_NAME:$DEFAULT_TAG
    - docker tag $IMAGE_NAME:$DEFAULT_TAG $IMAGE_NAME:$VERSIONED_TAG
    - docker tag $IMAGE_NAME:$DEFAULT_TAG $IMAGE_NAME:$LATEST_TAG
    - docker push $IMAGE_NAME:$VERSIONED_TAG
    - docker push $IMAGE_NAME:$LATEST_TAG
  needs: ["build tools-container image"]
  only:
    - main

# Metadata

test commit format:
  stage: test
  image: node:latest
  before_script: |
    if [ "${CI_COMMIT_BEFORE_SHA}" = "0000000000000000000000000000000000000000" ] || \
       [ ! -n "$(git branch -r --contains ${CI_COMMIT_BEFORE_SHA} 2> /dev/null)" ]
    then
        export COMMITLINT_FROM="HEAD^"
    else
        export COMMITLINT_FROM="${CI_COMMIT_BEFORE_SHA}"
    fi
  script:
    - npm install -g @commitlint/cli @commitlint/config-conventional
    - npx commitlint -x @commitlint/config-conventional -f "${COMMITLINT_FROM}"

# Docs

test docs:
  stage: test
  image: node:latest
  script:
    - npm install markdownlint-cli
    - npx markdownlint docs

.pages:
  image: python:3.8-buster
  before_script:
    - pip install -r requirements.docs.txt

test pages:
  extends: .pages
  stage: test
  script:
    - mkdocs build --strict --verbose --site-dir test
  artifacts:
    paths:
      - test
  except:
    - main

pages:
  extends: .pages
  stage: deploy
  script:
    - mkdocs build --strict --verbose
  artifacts:
    paths:
      - public
  only:
    - main


validate aws terraform config:
  image: $CI_REGISTRY_IMAGE/$TOOLS_CONTAINER_IMAGE_NAME:$TOOLS_CONTAINER_IMAGE_VERSION
  stage: test
  script:
    - cd provider-aws && tofu init -backend=false && tofu validate

validate do terraform config:
  image: $CI_REGISTRY_IMAGE/$TOOLS_CONTAINER_IMAGE_NAME:$TOOLS_CONTAINER_IMAGE_VERSION
  stage: test
  script:
    - cd provider-digitalocean && tofu init -backend=false && tofu validate


# Grove CLI
test Grove CLI:
  image: $CI_REGISTRY_IMAGE/$TOOLS_CONTAINER_IMAGE_NAME:$TOOLS_CONTAINER_IMAGE_VERSION
  stage: test
  variables:
    GIT_STRATEGY: none
    GROVE_TEMPLATE_BRANCH: main
  script:
    - git clone -b $GROVE_TEMPLATE_BRANCH https://gitlab.com/opencraft/dev/grove-template.git --recursive
    - pip install -r grove-template/defaults/requirements.txt --break-system-packages
    - cd grove-template/grove
    - git checkout $CI_COMMIT_BRANCH
    - cd tools-container/grove-cli/
    - make lint
    - CI_PROJECT_DIR="$(pwd)/../../../" make test.coverage
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: grove-template/grove/tools-container/grove-cli/coverage.xml
