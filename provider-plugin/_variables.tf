variable "provider_region" {
  type        = string
  description = "Region of the provider"
}

variable "cluster_name" {
  type        = string
  description = "Kubernetes cluster name"
}

variable "cluster_domain" {
  type        = string
  description = "Kubernetes cluster domain"
}

variable "cluster_id" {
  type        = string
  description = "Kubernetes cluster ID"
}


variable "cluster_config" {
  type        = string
  description = "Kubernetes cluster config (kubeconfig)"
}

variable "vpc_id" {
  type        = string
  description = "ID of the VPC"
}

variable "tutor_instances" {
  type        = list(string)
  description = "List of tutor instances"
}

variable "tutor_instance_buckets" {
  type        = map(any)
  description = "Tutor instances bucket name map"
}
variable "gitlab_cluster_agent_token" {
  type        = string
  description = "GitLab cluster agent token"
}
variable "gitlab_group_deploy_token_username" {
  type        = string
  description = "GitLab group deploy token username"
}
variable "gitlab_group_deploy_token_password" {
  type        = string
  description = "GitLab group deploy token password"
}

# DigitalOcean only
variable "project_id" {
  type        = string
  description = "DigitalOcean project ID"
  default     = ""
}

# DigitalOcean only
variable "cluster_urn" {
  type        = string
  description = "Kubernetes cluster URN"
  default     = ""
}

# AWS only
variable "cluster_arn" {
  type        = string
  description = "Kubernetes cluster ARN"
  default     = ""
}

variable "configs" {
  type        = object({})
  description = "Configs of instances that is stored in YAML files"
  default     = {}
}

variable "allowed_cors_origins" {
  type        = map(list(string))
  description = "Configs of instances that is stored in YAML files"
  default     = {}
}

variable "cluster_self_managed_node_groups" {
  type        = map(any)
  description = "Kubernetes cluster node groups"
  default     = {}
}
