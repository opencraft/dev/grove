####################################################################################################
## The Kubernetes Cluster itself
####################################################################################################

locals {
  cluster_provider               = "digitalocean"
  velero_aws_plugin_tag          = "v1.9.0" # https://github.com/vmware-tanzu/velero-plugin-for-aws/releases
  velero_digitalocean_plugin_tag = "v1.1.0" # https://github.com/digitalocean/velero-plugin/releases
}

# This _must_ be in a separate module in order for us to use the credentials it provides
# to then deploy additional resources onto the cluster.
# https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/kubernetes_cluster#kubernetes-terraform-provider-example
# "When using interpolation to pass credentials from a digitalocean_kubernetes_cluster resource to the Kubernetes provider, the cluster resource generally should not be created in the same Terraform module where Kubernetes provider resources are also used. "

module "k8s_cluster" {
  source = "./k8s-cluster"

  cluster_name          = var.cluster_name
  max_worker_node_count = var.max_worker_node_count
  min_worker_node_count = var.min_worker_node_count
  worker_node_size      = var.worker_node_size
  region                = var.do_region
  vpc_uuid              = digitalocean_vpc.main_vpc.id
  vpc_ip_range          = var.vpc_ip_range
}

# Declare the kubeconfig as an output - access it anytime with "/tf output -raw kubeconfig"
output "kubeconfig" {
  value     = module.k8s_cluster.kubeconfig.raw_config
  sensitive = true
}

####################################################################################################
## Integrate kubernetes cluster with the GitLab project
####################################################################################################

module "k8s_gitlab_connector" {
  source                     = "../provider-modules/k8s-gitlab-connector"
  gitlab_cluster_agent_token = var.gitlab_cluster_agent_token
}

####################################################################################################
## Create k8s secret for GitLab container registry access
####################################################################################################

module "k8s_gitlab_container_registry" {
  for_each = toset(var.tutor_instances)

  source = "../provider-modules/k8s-gitlab-container-registry"

  namespace                          = each.key
  container_registry_server          = var.container_registry_server
  dependency_proxy_server            = var.dependency_proxy_server
  gitlab_group_deploy_token_username = var.gitlab_group_deploy_token_username
  gitlab_group_deploy_token_password = var.gitlab_group_deploy_token_password
}

####################################################################################################
## Create monitoring pods
####################################################################################################

module "k8s_monitoring" {
  # Without the depends_on the cluster is deleted before this module when doing a `terraform destroy`.
  # Since it gets deleted before it's due, it'll cause errors.
  depends_on                         = [module.harmony]
  source                             = "../provider-modules/k8s-monitoring"
  cluster_domain                     = var.cluster_domain
  enable_monitoring_ingress          = var.enable_monitoring_ingress
  fluent_bit_aws_config              = yamldecode(var.fluent_bit_aws_config)
  fluent_bit_aws_kube_log_config     = yamldecode(var.fluent_bit_aws_kube_log_config)
  fluent_bit_aws_tracking_log_config = yamldecode(var.fluent_bit_aws_tracking_log_config)
  opensearch_index_retention_days    = var.opensearch_index_retention_days
  opensearch_persistence_size        = var.opensearch_persistence_size
  opensearch_resource_quotas         = lookup(yamldecode(var.k8s_resource_quotas), "monitoring-opensearch", {})
}

# The OpenSearch dashboard password - access it with "/tf output -raw opensearch_dashboard_admin_password"
output "opensearch_dashboard_admin_password" {
  value     = module.k8s_monitoring.opensearch_dashboard_admin_password
  sensitive = true
}

# The monitoring basic auth password - access it with "/tf output -raw monitoring_ingress_password"
output "monitoring_ingress_password" {
  value     = module.k8s_monitoring.monitoring_ingress_password
  sensitive = true
}

####################################################################################################
## Harmony integration
####################################################################################################

module "harmony" {
  source     = "../provider-modules/harmony"
  depends_on = [module.k8s_cluster.cluster_id]

  cluster_id                         = module.k8s_cluster.cluster_id
  cluster_domain                     = var.cluster_domain
  cluster_provider                   = local.cluster_provider
  openedx_instances                  = var.tutor_instances
  k8s_dashboard_enabled              = var.k8s_dashboard_enabled
  k8s_dashboard_host                 = var.k8s_dashboard_host
  lets_encrypt_notification_inbox    = var.lets_encrypt_notification_inbox
  ingress_resource_quota             = lookup(yamldecode(var.k8s_resource_quotas), "nginx", {})
  prometheus_enabled                 = var.prometheus_enabled
  prometheus_additional_alerts       = var.additional_prometheus_alerts
  grafana_enabled                    = var.grafana_enabled
  grafana_host                       = var.grafana_host
  alertmanager_enabled               = var.alertmanager_enabled
  alertmanager_config                = var.alertmanager_config
  velero_enabled                     = var.velero_enabled
  velero_backup_bucket               = module.velero_backups.spaces_bucket.name
  velero_backup_region               = module.velero_backups.spaces_bucket.region
  velero_backup_access_key_id        = var.do_access_key_id
  velero_backup_secret_access_key    = var.do_secret_access_key
  velero_plugin_aws_version          = local.velero_aws_plugin_tag
  velero_plugin_digitalocean_version = local.velero_digitalocean_plugin_tag
  velero_plugin_digitalocean_token   = var.do_token
  velero_volume_snapshot_provider    = "digitalocean.com/velero"
  openfaas_enabled                   = var.openfaas_enabled
  openfaas_host                      = var.openfaas_host
  elasticsearch_enabled              = var.shared_elasticsearch_enabled
  elasticsearch_config               = yamldecode(var.elasticsearch_config)
}

output "grafana_admin_password" {
  value     = module.harmony.grafana_admin_password
  sensitive = true
}

output "openfaas_admin_password" {
  value     = module.harmony.openfaas_admin_password
  sensitive = true
}

output "elasticsearch_ca_cert" {
  value     = module.harmony.elasticsearch_ca_cert
  sensitive = true
}
