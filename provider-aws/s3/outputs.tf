output "s3_bucket" {
  value = aws_s3_bucket.s3_bucket
}

output "s3_user_access_key" {
  value = aws_iam_access_key.s3_access_key
}

