####################################################################################################
## EdX Instance Configurations
####################################################################################################

module "edx_instances" {
  source = "../provider-modules/instance"

  tutor_instances     = var.tutor_instances
  tutor_instances_dir = pathexpand(var.tutor_instances_dir)
}