resource "mongodbatlas_cloud_backup_schedule" "backup_schedule" {
  count = var.backup_enabled ? 1 : 0

  project_id   = mongodbatlas_cluster.cluster.project_id
  cluster_name = mongodbatlas_cluster.cluster.name

  restore_window_days = var.backup_retention_period

  policy_item_daily {
    frequency_interval = 1
    retention_unit     = "days"
    retention_value    = var.backup_retention_period
  }
}

resource "mongodbatlas_cluster" "cluster" {
  # Basic Cluster Information
  depends_on             = [mongodbatlas_network_container.cluster_network_container]
  project_id             = var.mongodbatlas_project_id
  name                   = var.mongodbatlas_cluster_name
  mongo_db_major_version = var.mongodb_version

  # Topology Settings
  cluster_type = var.cluster_type
  replication_specs {
    num_shards = var.num_shards
    regions_config {
      priority        = 7
      region_name     = local.region_name
      electable_nodes = var.electable_nodes
      read_only_nodes = var.read_only_nodes
      analytics_nodes = var.analytics_nodes
    }
  }

  # Auto-Scaling Settings
  auto_scaling_disk_gb_enabled                    = var.auto_scaling_disk_gb_enabled
  auto_scaling_compute_enabled                    = var.auto_scaling_compute_enabled
  auto_scaling_compute_scale_down_enabled         = var.auto_scaling_compute_enabled
  provider_auto_scaling_compute_min_instance_size = var.auto_scaling_min_instances
  provider_auto_scaling_compute_max_instance_size = var.auto_scaling_max_instances

  # Provider Settings
  provider_name               = var.provider_name
  provider_region_name        = local.region_name
  disk_size_gb                = var.disk_size_gb
  provider_disk_iops          = var.disk_iops
  provider_volume_type        = var.volume_type
  cloud_backup                = var.backup_enabled
  provider_instance_size_name = var.instance_size
  encryption_at_rest_provider = (
    var.encryption_at_rest ?
    var.provider_name :
    "NONE"
  )
}

# Add the vpc CIDR block to the access list
resource "mongodbatlas_project_ip_access_list" "cluster_access_list" {
  project_id = var.mongodbatlas_project_id
  cidr_block = var.vpc_cidr_block
}

# Network container to define the MongoDB Atlas CIDR block
resource "mongodbatlas_network_container" "cluster_network_container" {
  project_id       = var.mongodbatlas_project_id
  atlas_cidr_block = var.atlas_cidr_block
  provider_name    = var.provider_name
  region_name      = local.region_name
}

# Peering between MongoDB Atlas and VPC
resource "mongodbatlas_network_peering" "cluster_network_peering" {
  project_id             = var.mongodbatlas_project_id
  container_id           = mongodbatlas_network_container.cluster_network_container.id
  accepter_region_name   = var.region_name
  provider_name          = var.provider_name
  route_table_cidr_block = var.vpc_cidr_block
  vpc_id                 = var.vpc_id
  aws_account_id         = var.aws_account_id
}

# Auto accept peering connection request
resource "aws_vpc_peering_connection_accepter" "accept_mongo_peer" {
  vpc_peering_connection_id = mongodbatlas_network_peering.cluster_network_peering.connection_id
  auto_accept               = true
}

# Add peering connection to private routing table
resource "aws_route" "peeraccess" {
  route_table_id            = var.route_table_id
  destination_cidr_block    = var.atlas_cidr_block
  vpc_peering_connection_id = mongodbatlas_network_peering.cluster_network_peering.connection_id
  depends_on = [
    aws_vpc_peering_connection_accepter.accept_mongo_peer
  ]
}
