resource "random_password" "user_passwords" {
  for_each = toset([
    for _, user in var.users :
    user.username
  ])

  length           = 16
  special          = true
  override_special = "$()-_[]{}<>"
}

resource "mongodbatlas_database_user" "users" {
  for_each = {
    for _, user in var.users :
    user.username => user
  }

  project_id         = var.mongodbatlas_project_id
  auth_database_name = "admin"

  username = each.key
  password = random_password.user_passwords[each.key].result

  roles {
    role_name     = "readAnyDatabase"
    database_name = "admin"
  }

  roles {
    role_name     = "readWrite"
    database_name = each.value.database
  }

  roles {
    role_name     = "readWrite"
    database_name = each.value.forum_database
  }

  scopes {
    type = "CLUSTER"
    name = mongodbatlas_cluster.cluster.name
  }
}
