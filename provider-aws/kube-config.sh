#! /bin/sh
AWS_KUBE_CONFIG=$(aws eks update-kubeconfig --name="$1" --region="$2" --dry-run)
echo '{"output":null }' | jq -rc ".output = \"$AWS_KUBE_CONFIG\n\""
