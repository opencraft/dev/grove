variable "cluster_name" { type = string }
variable "cluster_version" {
  type        = string
  default     = "1.30"
  description = "Kubernetes version for the cluster."
}

variable "min_worker_node_count" {
  type        = number
  default     = 3
  description = "Minimum number of running Kubernetes worker nodes."
}

variable "max_worker_node_count" {
  type        = number
  default     = 5
  description = "Maximum number of running Kubernetes worker nodes."
}

variable "worker_node_size" {
  type        = string
  default     = "t3.large"
  description = "Kubernetes worker node size."
}

variable "worker_node_volume_size" {
  type        = number
  default     = 20
  description = "Size of the worker node volume, in GB."
}

variable "cluster_private_subnets" {
  type        = string
  default     = "['10.0.1.0/24', '10.0.2.0/24', '10.0.5.0/24']"
  description = "Private subnets for the cluster."

  validation {
    condition     = can(yamldecode(var.cluster_private_subnets))
    error_message = "cluster_private_subnets must be valid YAML."
  }
}

variable "k8s_resource_quotas" {
  type = string
  validation {
    condition     = can(yamldecode(var.k8s_resource_quotas))
    error_message = "The k8s_resource_quotas provided was invalid."
  }
  description = "Resource configuration for the cluster."
  default     = "{}"
}

variable "aws_access_key" { type = string }
variable "aws_secret_access_key" { type = string }
variable "aws_region" { default = "us-east-1" }

variable "container_registry_server" { default = "registry.gitlab.com" }
variable "dependency_proxy_server" { default = "gitlab.com" }
variable "gitlab_group_deploy_token_username" { type = string }
variable "gitlab_group_deploy_token_password" { type = string }
variable "gitlab_cluster_agent_token" {
  type        = string
  description = "Token retrieved for Gitlab cluster agent"
}

variable "tutor_instances" {
  type = list(string)
}

variable "tutor_instances_dir" {
  type    = string
  default = "../../instances/"
}


variable "mongodbatlas_cluster_name" { default = "" }
variable "mongodbatlas_project_id" { type = string }
variable "mongodb_provider_name" { default = "AWS" }
variable "mongodb_version" { default = "7.0" }
variable "mongodb_instance_size" { default = "M10" }
variable "mongodb_cluster_type" { default = "REPLICASET" }
variable "mongodb_num_shards" { default = 1 }
variable "mongodb_electable_nodes" { default = 3 }
variable "mongodb_backup_enabled" { default = true }
variable "mongodb_backup_retention_period" { default = 35 }
variable "mongodb_encryption_at_rest" { default = false }
variable "mongodb_auto_scaling_disk_gb_enabled" { default = true }
variable "mongodb_auto_scaling_compute_enabled" { default = false }
variable "mongodb_read_only_nodes" { default = null }
variable "mongodb_analytics_nodes" { default = null }
variable "mongodb_disk_size_gb" { default = null }
variable "mongodb_disk_iops" { default = null }
variable "mongodb_volume_type" { default = null }
variable "mongodb_auto_scaling_min_instances" { default = null }
variable "mongodb_auto_scaling_max_instances" { default = null }
variable "mongodb_atlas_cidr_block" { default = "192.168.248.0/21" }

variable "rds_identifier" { default = "" }
variable "rds_instance_class" { default = "db.t3.micro" }
variable "rds_mysql_version" { default = "8.0" }
variable "rds_min_storage" { default = 10 }
variable "rds_max_storage" { default = 15 }
variable "rds_ca_cert_identifier" { default = null }

variable "rds_backup_retention_period" { default = 35 }

variable "rds_storage_encrypted" { default = true }

variable "rds_storage_alarm_enabled" { default = false }
variable "rds_storage_alarm_period" { default = 300 }
variable "rds_storage_alarm_evaluation_periods" { default = 1 }
variable "rds_storage_alarm_threshold" { default = 1000000000 }

variable "rds_storage_alarm_alarm_actions" {
  type        = string
  default     = "[]"
  description = "List of ARNs of actions to execute when the RDS storage alarm is triggered."

  validation {
    condition     = can(yamldecode(var.rds_storage_alarm_alarm_actions))
    error_message = "rds_storage_alarm_actions must be valid YAML."
  }
}

variable "k8s_dashboard_enabled" {
  type        = bool
  default     = false
  description = "Whether to enable the Kubernetes dashboard."
}

variable "k8s_dashboard_host" {
  type        = string
  description = "The host name for the Kubernetes dashboard"
  default     = null
}

variable "prometheus_enabled" {
  type        = bool
  default     = false
  description = "Whether to enable Prometheus monitoring."
}

variable "grafana_enabled" {
  type        = bool
  default     = false
  description = "Whether to enable Grafana monitoring."
}

variable "grafana_host" {
  type        = string
  description = "The host name for Grafana. Defaults to grafana.<cluster_domain>."
  default     = null
}

variable "alertmanager_enabled" {
  type        = bool
  default     = false
  description = "Whether to enable Alertmanager monitoring."
}

variable "alertmanager_config" {
  type        = string
  description = "Alert Manager configuration as a YAML-encoded string"
  default     = "{}"
  validation {
    condition     = can(yamldecode(var.alertmanager_config))
    error_message = "The alertmanager_config value must be a valid YAML-encoded string."
  }
}

variable "enable_monitoring_ingress" {
  type        = bool
  default     = false
  description = "Whether to enable ingress for monitoring services."
}

variable "openfaas_enabled" {
  type        = bool
  description = "Whether to enable OpenFAAS integration"
  default     = false
}

variable "openfaas_host" {
  type        = string
  description = "The host name for OpenFAAS Gateway"
  default     = null
}

variable "cluster_domain" {
  type        = string
  default     = "grove.local"
  description = "Domain used as the base for monitoring services."
}

variable "lets_encrypt_notification_inbox" {
  type        = string
  default     = "contact@example.com"
  description = "Email to send any email notifications about Letsencrypt"
}

variable "global_404_html_path" {
  type        = string
  default     = ""
  description = "Path in tools-container to the html page to show when provisioning instances or if there's a 404 on the ingress."
}

variable "ami_id" {
  type        = string
  default     = ""
  description = "AMI ID for the given AWS region to use for the worker nodes. Get from https://cloud-images.ubuntu.com/aws-eks/."
}

variable "enable_shared_elasticsearch" {
  type        = bool
  default     = false
  description = "Enable the shared Elasticsearch cluster."
}

variable "fluent_bit_aws_config" {
  type        = string
  default     = "{}"
  description = "AWS credentials for Fluent-bit to use when uploading logs to S3 or Cloudwatch."

  validation {
    condition     = can(yamldecode(var.fluent_bit_aws_config))
    error_message = "The fluent_bit_aws_config provided was invalid."
  }
}

variable "fluent_bit_aws_tracking_log_config" {
  type        = string
  default     = "{}"
  description = "Fluent-bit configuration for uploading tracking logs to an S3-compatible backend or Cloudwatch."

  validation {
    condition     = can(yamldecode(var.fluent_bit_aws_tracking_log_config))
    error_message = "The fluent_bit_aws_tracking_log_config provided was invalid."
  }
}

variable "fluent_bit_aws_kube_log_config" {
  type        = string
  default     = "{}"
  description = "Fluent-bit configuration for uploading kubernetes logs to an S3-compatible backend or Cloudwatch."

  validation {
    condition     = can(yamldecode(var.fluent_bit_aws_kube_log_config))
    error_message = "The fluent_bit_aws_kube_log_config provided was invalid."
  }
}

variable "elasticsearch_config" {
  type = string
  validation {
    condition     = can(yamldecode(var.elasticsearch_config))
    error_message = "The elasticsearch_config provided was invalid."
  }
  description = "Configuration for the shared ElasticSearch cluster."
  default     = "{}"
}

variable "opensearch_index_retention_days" {
  type        = number
  default     = 180
  description = "Number of days to retain index data in OpenSearch."
}

variable "opensearch_persistence_size" {
  type        = string
  default     = "8Gi"
  description = "Size of the persistent volume claim for OpenSearch."
}

variable "additional_prometheus_alerts" {
  type        = string
  description = "Additional PrometheusRule alerts in YAML format."
  default     = ""
  validation {
    condition     = length(trimspace(var.additional_prometheus_alerts)) == 0 || can(yamldecode(var.additional_prometheus_alerts))
    error_message = "The additional_prometheus_alerts variable must be valid YAML or an empty string."
  }
}

variable "velero_enabled" {
  type        = bool
  description = "Whether to enable Velero backup"
  default     = false
}

variable "shared_elasticsearch_enabled" {
  type        = bool
  description = "Whether to enable the shared Elasticsearch"
  default     = false
}
